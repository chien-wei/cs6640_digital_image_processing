function T = CS6640_FFT_radial(im)
% CS6640_FFT_radial - compute FFT radial texture parameters
% On input:
%     im (MxN array): input image
% On output:
%     T (M*Nx10 array): texture parameters
%     each texture parameter is a column vector in T
% Call:
%     T = CS6640_FFT_radial(im);
% Author:
%     Kenway
%     UU
%     Fall 2018
%

belong = zeros(19,19);
tmp = [10, 10, 10, 10];
for i=1:10
    belong(tmp(1), :) = ones(1, 19).*i;
    belong(:, tmp(2)) = ones(19, 1).*i;
    belong(tmp(3), :) = ones(1, 19).*i;
    belong(:, tmp(4)) = ones(19, 1).*i;
    tmp(1) = tmp(1) - 1;
    tmp(2) = tmp(2) - 1;
    tmp(3) = tmp(3) + 1;
    tmp(4) = tmp(4) + 1;
end

% adding black padding
imp = padarray(im,[9 9],'both');

[M, N] = size(im);
T = zeros(M, N, 10);
for r = 1:M
    for c = 1:N
        r2 = r+9;
        c2 = c+9;
        F = fft2(imp(r2-9:r2+9, c2-9:c2+9));
        ps = F.*conj(F);
        t = zeros(1, 10);
        for psr = 1:19
            for psc = 1:19
                b = belong(psr, psc);
                t(b) = t(b) + ps(psr, psc);
            end
        end
        T(r, c, :) = t(:);
    end
end

T = reshape(T, [M*N, 10]);