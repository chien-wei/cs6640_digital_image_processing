function Bu = CS6640_basis_FT1D(u, N)


Bu = zeros(1, N);

for x = 1:N
    Bu(x) = exp(-j*2*pi*(u*x/N));
end
