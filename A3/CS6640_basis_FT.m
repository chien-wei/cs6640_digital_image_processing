function Buv = CS6640_basis_FT(u, v, M, N)

Buv = zeros(M, N);

for x = 1:M
    for y = 1:N
        Buv(x, y) = exp(-j*2*pi*(u*x/M+v*y/N));
    end
end