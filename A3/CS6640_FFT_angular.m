function T = CS6640_FFT_angular(im)
% CS6640_FFT_angular - compute FFT angular texture parameters
% On input:
%     im (MxN array): input image
% On output:
%     T (M*Nx8 array): texture parameters
%     each texture parameter is a column vector in T
% Call:
%     T = CS6640_FFT_angular(im);
% Author:
%     Kenway
%     UU
%     Fall 2018
%
k = 3;
K = 2*k + 1;
belong = zeros(K,K);
for r=1:K
    for c=1:K
        belong(r,c) = 1 + ...
        fix(wrapTo360(rad2deg(atan2(r - k - 1, c - k - 1))) / 45);
    end
end

% adding black padding
imp = padarray(im,[k, k],'both');

[M, N] = size(im);
T = zeros(M, N, 8);
for r = 1:M
    for c = 1:N
        r2 = r+k;
        c2 = c+k;
        F = fft2(imp(r2-k:r2+k, c2-k:c2+k));
        ps = F.*conj(F);
        t = zeros(1, 8);
        for psr = 1:K
            for psc = 1:K
                b = belong(psr, psc);
                t(b) = t(b) + ps(psr, psc);
            end
        end
        T(r, c, :) = t(:);
    end
end

T = reshape(T, [M*N, 8]);