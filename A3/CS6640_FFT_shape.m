function X = CS6640_FFT_shape(Z,w)
% CS6640_FFT_shape - compute Fourier shape descriptors for a curve
% On input:
%     Z (Nx2 array): input curve (should be closed)
%     w (int): distance along curve to determine angles
% On output:
%     X ((N/2-1)x1 vector): the Fourier coefficients for the curve
% Call:
%     X = CS6640_FFT_shape(curve,2);
% Author:
%     Kenway
%     UU
%     Fall 2018
%

[N, c] = size(Z);
theta = zeros(N, 1);
phi = zeros(N, 1);
psi = zeros(N, 1);

% L is actually no need in here. We consider angle instead of length.
% Also we can see the t in phi(t) part as discrete [1:N].
% L = 0;
% Compute L
%for t=1:N
%    if t-1 < 1
%        delta_y = Z(t, 2) - Z(t+N-1, 2);
%        delta_x = Z(t, 1) - Z(t+N-1, 1);
%        theta(t) = atan2(Z(t, 2) - Z(t+N-1, 2), Z(t, 1) - Z(t+N-1, 1)); 
%    else
%        delta_y = Z(t, 2) - Z(t-1, 2);
%        delta_x = Z(t, 1) - Z(t-1, 1);
%    end
%    L = L + sqrt(delta_x * delta_x + delta_y * delta_y);
%end

% Get theta using atan2(delta_y, delta_x)
for t=1:N
    if t-w < 1
        delta_y = Z(t, 2) - Z(t+N-w, 2);
        delta_x = Z(t, 1) - Z(t+N-w, 1); 
    else
        delta_y = Z(t, 2) - Z(t-w, 2);
        delta_x = Z(t, 1) - Z(t-w, 1);
    end
    theta(t) = atan2(delta_y, delta_x);
end
% fix atan2 domain problem on -2*pi
% using cross to get direction, acos of unit vector to get angle
v1 = [cos(theta(N)), sin(theta(N)), 0];
v2 = [cos(theta(1)), sin(theta(1)), 0];
dir = cross(v1, v2);
dir = dir(3);
theta(1) = theta(N) + acos(dot(v1, v2)) * sign(dir);
for t=1:N-1
    v1 = [cos(theta(t)), sin(theta(t)), 0];
    v2 = [cos(theta(t+1)), sin(theta(t+1)), 0];
    dir = cross(v1, v2);
    dir = dir(3);
    theta(t+1) = theta(t) + acos(dot(v1, v2)) * sign(dir);
end
% Get phi, cumulative angluar function
for t=1:N
    phi(t) = mod(theta(t) - theta(1), 2 * pi);
end
% Get Tom's version of psi
for t=1:N
    psi(t) = mod(phi(t) + t*2*pi/N, pi);
end
X = fft(psi);
% power
X = X .* conj(X);
X = X(2: uint8(N/2));