function T = CS6640_FFT_texture(im)
% CS6640_FFT_texture - compute FFT texture parameters
% On input:
%     im (MxN array): input image
% On output:
%     T (M*Nx25 array): texture parameters
%      each texture parameter is a column vector in T
% Call:
%     T = CS6640_FFT_texture(im);
% Author:
%     Kenway
%     UU
%     Fall 2018
%

% adding black padding
imp = padarray(im,[2 2],'both');

[M, N] = size(im);
T = zeros(M, N, 25);
for r = 1:M
    for c = 1:N
        r2 = r+2;
        c2 = c+2;
        F = fft2(imp(r2-2:r2+2, c2-2:c2+2));
        ps = F.*conj(F);
        T(r, c, :) = ps(:);
    end
end

T = reshape(T, [M*N, 25]);
