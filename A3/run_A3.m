function run_A3
% run_A3 - Execute all A3 function. This code will show result in 
% figures
% On input:
%     Null
% On output:
%     Null
% Call:
%     run_A3
% Author:
%     Kenway
%     UU
%     Fall 2018
%

im1 = imread('im1.jpg');

T = CS6640_FFT_texture(rgb2gray(im1));
[cidx,ctrs] = kmeans(T,4);
combo(mat2gray(rgb2gray(im1)),(reshape(cidx==1,480,640)));
combo(mat2gray(rgb2gray(im1)),(reshape(cidx==2,480,640)));
combo(mat2gray(rgb2gray(im1)),(reshape(cidx==3,480,640)));
combo(mat2gray(rgb2gray(im1)),(reshape(cidx==4,480,640)));


T = CS6640_FFT_radial(rgb2gray(im1));
[cidx,ctrs] = kmeans(T,4);
combo(mat2gray(rgb2gray(im1)),(reshape(cidx==1,480,640)));
combo(mat2gray(rgb2gray(im1)),(reshape(cidx==2,480,640)));
combo(mat2gray(rgb2gray(im1)),(reshape(cidx==3,480,640)));
combo(mat2gray(rgb2gray(im1)),(reshape(cidx==4,480,640)));


T = CS6640_FFT_angular(rgb2gray(im1));
[cidx,ctrs] = kmeans(T,4);
combo(mat2gray(rgb2gray(im1)),(reshape(cidx==1,480,640)));
combo(mat2gray(rgb2gray(im1)),(reshape(cidx==2,480,640)));
combo(mat2gray(rgb2gray(im1)),(reshape(cidx==3,480,640)));
combo(mat2gray(rgb2gray(im1)),(reshape(cidx==4,480,640)));

% Question 4
H = [2,10; 3,10; 4,10; 4,9; 4,8; 4,7; 5,7; 6,7; 7,7; 7,8; 7,9; 7,10;...
    8,10; 9,10; 9,9; 9,8; 9,7; 9,6; 9,5; 9,4; 9,3; 9,2; 8,2; 7,2; ...
    7,3; 7,4; 7,5; 6,5; 5,5; 4,5; 4,4; 4,3; 4,2; 3,2; 2,2; 2,3; 2,4;...
    2,5; 2,6; 2,7; 2,8; 2,9; 2,10];

O = [6,17; 7, 17; 8,17; 9,17; 10,17; 11,17; 12,17; 13,16; 14,15;...
    15,14; 16,13; 17,12; 17,11; 17,10; 17,9; 17,8; 17,7; 16,6; ...
    15,5; 14,4; 13,3; 13,2; 12,2; 11,2; 10,2; 9,2; 8,2; 7,2; 6,3;...
    5,4; 4,5; 3,6; 2,6; 2,7; 2,8; 2,9; 2,10; 2,11; 2,12; 3,13; ...
    4,14; 5,15; 6,16; 7,17];

Z = O;
w = 6;
[N, c] = size(Z);

x = Z(:, 1);
y = Z(:, 2);
scatter(x,y); % use scatter to draw shape :p
hold on
axis equal
plot(0, 0);
plot(11, 11);

% X is original Z descriptor
X = CS6640_FFT_shape(Z, w);

Mr = [cosd(45) -sind(45) 0; sind(45), cosd(45) 0 ; 0 0 1];
Zr = zeros(N, 1);
Zr = [Z Zr];
Zr = Zr * Mr;
% Y is Z that rotate 45 degree
Y = CS6640_FFT_shape(Zr, w);
scatter(Zr(:,1), Zr(:,2));

Ms = [0.5 0 0; 0 0.5 0 ; 0 0 1];
Zs = zeros(N, 1);
Zs = [Z Zs];
Zs = Zs * Ms;
% V is Z that scale in 0.5
V = CS6640_FFT_shape(Zs, w);
scatter(Zs(:,1), Zs(:,2));

Zsr = zeros(N, 1);
Zsr = [Z Zsr];
Zsr = Zsr * Mr * Ms;
% W is smaller Z with rotation
W = CS6640_FFT_shape(Zsr, w);
scatter(Zsr(:, 1), Zsr(:, 2));

% all result are zeros
X-Y
X-V
X-W
