function s = CS6640_object_features(MM)%, video)
% CS6640_object_features - another feature collection helper
% On input:
%     M (Matlab movie): movie of segmented moving objects
%     vidObj (video object obtained by VideoReader): input video
% On output:
%     s (nxm array): n samples of m-tuple feature vectors
% Call:
%     CS6640_object_features(MM);
% Author:
%     Kenway Sun
%     Fall 2018
%     UU
%
L = size(MM, 2);
N = size(MM(1).cdata, 2);
s = zeros(L,5);
%i = 0;
%while hasFrame(video)
%    vidFrame = readFrame(video);
    
%    i = i + 1;
for i=1:L
    stats = regionprops(imbinarize(rgb2gray(MM(i).cdata)), ...
    'Centroid', 'BoundingBox', 'Area', 'Solidity');
    s(i, 1:2) = stats.BoundingBox(3: 4);
    s(i, 3) = stats.Area / (2 - stats.Centroid(1)/N);
    s(i, 4) = stats.Solidity;
    perimeter = size(find(bwperim(imbinarize(rgb2gray(MM(i).cdata)))), 1);
    s(i, 5) = perimeter * perimeter / s(i, 3);
end