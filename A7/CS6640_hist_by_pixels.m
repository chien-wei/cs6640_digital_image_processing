function [hist, bins] = CS6640_hist_by_pixels(pixels, im, num_bin)
% CS6640_hist_by_pixels - get value by pixels and return histogram
% On input:
%     pixels (mx1 array): pixels index in im
%     im (MxN array): (uint8)image
%     num_bin: (int): number of bin n for distribution
% On output:
%     hist (nx1 int vector): histogram value for each bin
%     bins (nx1 float vector): boundary value of each bin
% Call:
%     [hist, bins] = CS6640_hist_by_pixels(pixels, im, num_bin);
% Author:
%     Kenway Sun
%     Fall 2018
%     UU
%

bins = zeros(num_bin, 1);
for i=1:num_bin
    bins(i) = i * 256.0 / num_bin;
end

hist = zeros(num_bin, 1);

N = size(pixels, 1);
for i=1:N
    for j=1:num_bin
        if im(pixels(i)) < bins(j)
            hist(j) = hist(j) + 1;
            break
        end
    end
end