% Pre-processing

V_t1 = VideoReader('truck1.avi');
V_t2 = VideoReader('truck2.avi');
V_t3 = VideoReader('truck3.avi');
V_c1 = VideoReader('car1.avi');
V_c2 = VideoReader('car2.avi');
V_c3 = VideoReader('car3.avi');
V_b1 = VideoReader('bus1.avi');
V_b2 = VideoReader('bus2.avi');
V_b3 = VideoReader('bus3.avi');
%M_t1 = CS6640_MM_A7(V_t1);
%M_t2 = CS6640_MM_A7(V_t2);
%M_t3 = CS6640_MM_A7(V_t3);
%M_c1 = CS6640_MM_A7(V_c1);
%M_c2 = CS6640_MM_A7(V_c2);
%M_c3 = CS6640_MM_A7(V_c3);
%M_b1 = CS6640_MM_A7(C_b1);
%M_b2 = CS6640_MM_A7(C_b2);
%M_b3 = CS6640_MM_A7(C_b3);
load MMs

% bus
size_M_b1 = size(M_b1, 2);
size_M_b2 = size(M_b2, 2);
size_M_b3 = size(M_b3, 2);

%bus1 = CS6640_object_size(M_b1);
%bus2 = CS6640_object_size(M_b2);
%bus3 = CS6640_object_size(M_b3);
%plot(bus1, 'LineWidth',1)
%hold on
%plot(bus2, 'LineWidth',1)
%plot(bus3, 'LineWidth',1)

%title('bus size', 'FontSize', 14)
%xlabel('frame', 'FontSize', 14)
%ylabel('size (number of pixel)', 'FontSize', 14)

% truck
size_M_t1 = size(M_t1, 2);
size_M_t2 = size(M_t2, 2);
size_M_t3 = size(M_t3, 2);

%truck1 = CS6640_object_size(M_t1);
%truck2 = CS6640_object_size(M_t2);
%truck3 = CS6640_object_size(M_t3);

%plot(truck1, 'LineWidth',3)
%hold on
%plot(truck2, 'LineWidth',3)
%plot(truck3, 'LineWidth',3)

%title('truck size', 'FontSize', 14)
%xlabel('frame', 'FontSize', 14)
%ylabel('size (number of pixel)', 'FontSize', 14)

% car
size_M_c1 = size(M_c1, 2);
size_M_c2 = size(M_c2, 2);
size_M_c3 = size(M_c3, 2);

%car1 = CS6640_object_size(M_c1);
%car2 = CS6640_object_size(M_c2);
%car3 = CS6640_object_size(M_c3);

%plot(car1, 'LineWidth',1)
%hold on
%plot(car2, 'LineWidth',1)
%plot(car3, 'LineWidth',1)

%title('car size', 'FontSize', 14)
%xlabel('frame', 'FontSize', 14)
%ylabel('size (number of pixel)', 'FontSize', 14)

% hist
car1_hist = CS6640_object_hist(V_c1, M_c1);
car2_hist = CS6640_object_hist(V_c2, M_c2);
car3_hist = CS6640_object_hist(V_c3, M_c3);
bus1_hist = CS6640_object_hist(V_b1, M_b1);
bus2_hist = CS6640_object_hist(V_b2, M_b2);
bus3_hist = CS6640_object_hist(V_b3, M_b3);
truck1_hist = CS6640_object_hist(V_t1, M_t1);
truck2_hist = CS6640_object_hist(V_t2, M_t2);
truck3_hist = CS6640_object_hist(V_t3, M_t3);

% feature
car1_feat = CS6640_object_features(M_c1);
car2_feat = CS6640_object_features(M_c2);
car3_feat = CS6640_object_features(M_c3);
bus1_feat = CS6640_object_features(M_b1);
bus2_feat = CS6640_object_features(M_b2);
bus3_feat = CS6640_object_features(M_b3);
truck1_feat = CS6640_object_features(M_t1);
truck2_feat = CS6640_object_features(M_t2);
truck3_feat = CS6640_object_features(M_t3);

% final feature
%car1 = [car1_hist, car1_feat];
%car2 = [car2_hist, car2_feat];
%car3 = [car3_hist, car3_feat];
%bus1 = [bus1_hist, bus1_feat];
%bus2 = [bus2_hist, bus2_feat];
%bus3 = [bus3_hist, bus3_feat];
%tru1 = [truck1_hist, truck1_feat];
%tru2 = [truck2_hist, truck2_feat];
%tru3 = [truck3_hist, truck3_feat];

car1 = car1_feat;
car2 = car2_feat;
car3 = car3_feat;
bus1 = bus1_feat;
bus2 = bus2_feat;
bus3 = bus3_feat;
tru1 = truck1_feat;
tru2 = truck2_feat;
tru3 = truck3_feat;

% car1, car2, bus1, bus2, tru1, tru2 as training set
N = size_M_c1 + size_M_b1 + size_M_t1 + ...
    size_M_c2 + size_M_b2 + size_M_t2;
class_probs = [size_M_c1 + size_M_c2, ...
               size_M_b1 + size_M_b2, ...
               size_M_t1 + size_M_t2]/N;
           
class_models = [CS6640_build_Bayes([car1', car2']'), ...
                CS6640_build_Bayes([bus1', bus2']'), ...
                CS6640_build_Bayes([tru1', tru2']')];
            
            
% car3, bus3, tru3 as testing set
test_car3 = zeros(size_M_c3, 1);
for i=1:size_M_c3
    x = car3(i, :)';
    test_car3(i) = CS6640_Bayes(x, class_probs, class_models);
end
length(find(test_car3 == 1))/length(test_car3)
% 0.89
% without adjust on size with col index 0.88
% with 0.96
% without solid 0.98

test_bus3 = zeros(size_M_b3, 1);
for i=1:size_M_b3
    x = bus3(i, :)';
    test_bus3(i) = CS6640_Bayes(x, class_probs, class_models);
end
length(find(test_bus3 == 2))/length(test_bus3)
% 1
% without adjust on size with col index 0.96
% with 1
% without solid 1

test_tru3 = zeros(size_M_t3, 1);
for i=1:size_M_t3
    x = tru3(i, :)';
    test_tru3(i) = CS6640_Bayes(x, class_probs, class_models);
end
length(find(test_tru3 == 3))/length(test_tru3)
% 0.38
% without adjust on size with col index 0.37
% 0.4
% without solid 0.33

test_tru2 = zeros(size_M_t2, 1);
for i=1:size_M_t2
    x = tru2(i, :)';
    test_tru2(i) = CS6640_Bayes(x, class_probs, class_models);
end
length(find(test_tru2 == 3))/length(test_tru2)
% 1
% without adjust on size with col index 0.88
% with 0.76
% without solid 0.82

test_tru1 = zeros(size_M_t1, 1);
for i=1:size_M_t1
    x = tru1(i, :)';
    test_tru1(i) = CS6640_Bayes(x, class_probs, class_models);
end
length(find(test_tru1 == 3))/length(test_tru1)
% 0.56
% without adjust on size with col index 0.37
% with 0.82
% without solid 0.55


% 11/27 update
MM_b1 = M_b1(1:200);
MM_b2 = M_b2(1:200);
MM_b3 = M_b3(1:255);
MM_c1 = M_c1(1:130);
MM_c2 = M_c2(35:226);
MM_c3 = M_c3(1:95);
MM_t1 = M_t1(1:130);
MM_t2 = M_t2(1:167);
MM_t3 = M_t3(1:192);

size_MM_b1 = size(MM_b1, 2);
size_MM_b2 = size(MM_b2, 2);
size_MM_b3 = size(MM_b3, 2);
size_MM_c1 = size(MM_c1, 2);
size_MM_c2 = size(MM_c2, 2);
size_MM_c3 = size(MM_c3, 2);
size_MM_t1 = size(MM_t1, 2);
size_MM_t2 = size(MM_t2, 2);
size_MM_t3 = size(MM_t3, 2);

car1_feat = CS6640_object_features(MM_c1);
car2_feat = CS6640_object_features(MM_c2);
car3_feat = CS6640_object_features(MM_c3);
bus1_feat = CS6640_object_features(MM_b1);
bus2_feat = CS6640_object_features(MM_b2);
bus3_feat = CS6640_object_features(MM_b3);
truck1_feat = CS6640_object_features(MM_t1);
truck2_feat = CS6640_object_features(MM_t2);
truck3_feat = CS6640_object_features(MM_t3);

car1 = car1_feat;
car2 = car2_feat;
car3 = car3_feat;
bus1 = bus1_feat;
bus2 = bus2_feat;
bus3 = bus3_feat;
tru1 = truck1_feat;
tru2 = truck2_feat;
tru3 = truck3_feat;

% car1, car2, bus1, bus2, tru1, tru2 as training set
N = size_MM_c1 + size_MM_b1 + size_MM_t1 + ...
    size_MM_c2 + size_MM_b2 + size_MM_t2;
class_probs = [size_MM_c1 + size_MM_c2, ...
               size_MM_b1 + size_MM_b2, ...
               size_MM_t1 + size_MM_t2]/N;
           
class_models = [CS6640_build_Bayes([car1', car2']'), ...
                CS6640_build_Bayes([bus1', bus2']'), ...
                CS6640_build_Bayes([tru1', tru2']')];
            
test_car3 = zeros(size_MM_c3, 1);
for i=1:size_MM_c3
    x = car3(i, :)';
    test_car3(i) = CS6640_Bayes(x, class_probs, class_models);
end
length(find(test_car3 == 1))/length(test_car3)
length(find(test_car3 == 2))/length(test_car3)
length(find(test_car3 == 3))/length(test_car3)

test_bus3 = zeros(size_MM_b3, 1);
for i=1:size_MM_b3
    x = bus3(i, :)';
    test_bus3(i) = CS6640_Bayes(x, class_probs, class_models);
end
length(find(test_bus3 == 1))/length(test_bus3)
length(find(test_bus3 == 2))/length(test_bus3)
length(find(test_bus3 == 3))/length(test_bus3)

test_tru3 = zeros(size_MM_t3, 1);
for i=1:size_MM_t3
    x = tru3(i, :)';
    test_tru3(i) = CS6640_Bayes(x, class_probs, class_models);
end
length(find(test_tru3 == 1))/length(test_tru3)
length(find(test_tru3 == 2))/length(test_tru3)
length(find(test_tru3 == 3))/length(test_tru3)


% training set
train_car = zeros(size_MM_c1 + size_MM_c2, 1);
for i=1:size_MM_c1
    x = car1(i, :)';
    train_car(i) = CS6640_Bayes(x, class_probs, class_models);
end
for i=(1+size_MM_c1):(size_MM_c1+size_MM_c2)
    x = car2(i - size_MM_c1, :)';
    train_car(i) = CS6640_Bayes(x, class_probs, class_models);
end
length(find(train_car == 1))/length(train_car)
length(find(train_car == 2))/length(train_car)
length(find(train_car == 3))/length(train_car)

train_bus = zeros(size_MM_b1 + size_MM_b2, 1);
for i=1:size_MM_b1
    x = bus1(i, :)';
    train_bus(i) = CS6640_Bayes(x, class_probs, class_models);
end
for i=(1+size_MM_b1):(size_MM_b1+size_MM_b2)
    x = bus2(i - size_MM_b1, :)';
    train_bus(i) = CS6640_Bayes(x, class_probs, class_models);
end
length(find(train_bus == 1))/length(train_bus)
length(find(train_bus == 2))/length(train_bus)
length(find(train_bus == 3))/length(train_bus)

train_tru = zeros(size_MM_t1 + size_MM_t2, 1);
for i=1:size_MM_t1
    x = tru1(i, :)';
    train_tru(i) = CS6640_Bayes(x, class_probs, class_models);
end
for i=(1+size_MM_t1):(size_MM_t1+size_MM_t2)
    x = tru2(i - size_MM_t1, :)';
    train_tru(i) = CS6640_Bayes(x, class_probs, class_models);
end
length(find(train_tru == 1))/length(train_tru)
length(find(train_tru == 2))/length(train_tru)
length(find(train_tru == 3))/length(train_tru)

mean_c = class_models(1).mean
mean_b = class_models(2).mean
mean_t = class_models(3).mean
var_c = class_models(1).var
var_b = class_models(2).var
var_t = class_models(3).var

norm_c = normpdf([0:5000], mean_c(3), var_c(3,3)/500);
norm_b = normpdf([0:5000], mean_b(3), var_b(3,3)/3000);
norm_t = normpdf([0:5000], mean_t(3), var_t(3,3)/3000);

plot(norm_c,'Linewidth',3)
hold on
plot(norm_b,'Linewidth',3)
plot(norm_t,'Linewidth',3)