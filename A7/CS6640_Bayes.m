function best_class = CS6640_Bayes(x,class_probs,class_models)
% CS6640_Bayes - Bayes classifier
% On input:
%     x (mx1 vector): feature vector
%     class_pros (1xn vector): probabilities of n classes (sums to 1)
%     class_models (1xn vector struct): class models: means and
%     variances
%     (k).mean (mx1 vector): k_th class mean vector
%     (k).var (mxm array): k_th class covariance matrix
% On output:
%     best_class (int): index of best class for x
% Call:
%     c = CS6640_Bayes(x, cp, cm);
% Author:
%     Kenway Sun
%     UU
%     Fall 2018
%

warning('off','all');
num_class = size(class_probs, 2);
n = length(x);
p = zeros(1, num_class);

for i=1: num_class
    m = class_models(i).mean;
    C = class_models(i).var;
    term1 = ((2*pi) ^ (n/2)) * (det(C) ^ (1/2));
    term2 = -(x - m)' * inv(C) * (x - m) / 2;
    p(i) = exp(term2)/term1 * class_probs(i);
end

[max_p, best_class] = max(p);


