function im2 = CS6640_bw_largest(im)
% CS6640_bw_largest - return the same binary image with only largest blob 
% On input:
%     im (MxN array): binary image
% On output:
%     im2 (MxN array): binary image with only one blob
% Call:
%     im2 = CS6640_bw_largest(im)
% Author:
%     Kenway Sun
%     Fall 2018
%     UU
%

[imlabel, num_label] = bwlabel(im);
largest = 1;
area = 0;

for i=1:num_label
    s = size(find(imlabel==i), 1);
    if s > area
        area = s;
        largest = i;
    end
end

im2 = imlabel==largest;