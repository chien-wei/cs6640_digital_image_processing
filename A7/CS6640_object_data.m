function object_data = CS6640_object_data(M,vidObj)
%
% On input:
%     M (Matlab movie): movie of segmented moving objects
%     vidObj (video object obtained by VideoReader): input video
% On output:
%     object_data (struct vector): object data
%     (k).num_objects (int): number of objects in frame k
%     (k).objects (struct vector): has features for each object (p)
%     objects(p).row_mean (float): row mean
%     objects(p).col_mean (float): column mean
%     objects(p).ul_row (int): upper left row for bounding box
%     objects(p).ul_col (int): upper left col for bounding box
%     objects(p).lr_row (int): lower right row for bounding box
%     objects(p).lr_col (int): lower right col for bounding box
%     objects(p).num_pixels (int): number of pixels
%     objects(p).red_median (int): median red value for object
%     objects(p).green_median (int): median green value for object
%     objects(p).blue_median (int): median blue value for object
% Call:
%     obj_data = CS6640_object_data(M,vidObj);
% Author:
%     Kenway Sun
%     Fall 2018
%     UU
%

H = vidObj.Height;
W = vidObj.Width;
L = vidObj.Duration * vidObj.FrameRate;
object_data(L) = struct();
i = 0;
while hasFrame(vidObj)
    i = i + 1;
    vidFrame = readFrame(vidObj);
    
    % BW = bwlabel(rgb2gray(M(i).cdata));
    % Matlab says regionprops is better with logical than with bwlabel
    BW = logical(rgb2gray(M(i).cdata));
    
    % show image
    %imshow(BW);
    %hold on;
    
    objs = regionprops(BW,'centroid', 'BoundingBox', ...
                          'Area', 'PixelIdxList');
    [num_obj, z] = size(objs);
    object_data(i).num_objects = num_obj;
    object_data(i).objects(num_obj) = struct();
    
    for j=1:num_obj
        object_data(i).objects(j).row_mean = objs(j).Centroid(1);
        object_data(i).objects(j).col_mean = objs(j).Centroid(2);
        object_data(i).objects(j).ul_row = round(objs(j).BoundingBox(1));
        object_data(i).objects(j).ul_col = round(objs(j).BoundingBox(2));
        object_data(i).objects(j).lr_row = ...
            round(objs(j).BoundingBox(1) + objs(j).BoundingBox(3));
        object_data(i).objects(j).lr_col = ... 
            round(objs(j).BoundingBox(2) + objs(j).BoundingBox(4));
        object_data(i).objects(j).num_pixels = round(objs(j).Area(1));
        
        % index
        num_pix = round(objs(j).Area(1));
        pix = zeros(num_pix, 2);
        [pix(:, 1), pix(:, 2)] = ind2sub([H, W], objs(j).PixelIdxList);
        r_value = uint8(zeros(num_pix, 1));
        g_value = uint8(zeros(num_pix, 1));
        b_value = uint8(zeros(num_pix, 1));
        for k=1:object_data(i).objects(j).num_pixels
            r_value(k) = vidFrame(pix(k, 1), pix(k, 2), 1);
            g_value(k) = vidFrame(pix(k, 1), pix(k, 2), 2);
            b_value(k) = vidFrame(pix(k, 1), pix(k, 2), 3);
        end
        object_data(i).objects(j).red_median = round(median(r_value));
        object_data(i).objects(j).green_median = round(median(g_value));
        object_data(i).objects(j).blue_median = round(median(b_value));
        
        % draw bounding box
        %o = object_data(i).objects(j);
        %rectangle('Position',[o.ul_row, ...
        %                      o.ul_col, ... 
        %                      o.lr_row - o.ul_row, ...
        %                      o.lr_col - o.ul_col], ...
        %          'EdgeColor','b', ...
        %          'LineWidth',3);
    end
    
    %hold off;
    %clf;
end
