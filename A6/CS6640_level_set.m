function [phi,tr] = CS6640_level_set(im,max_iter,del_t,r0,c0)
% CS6640_level_set - level set of image
% On input:
%     im (MxN array): gray level or binary image
%     max_iter (int): maximum number of iterations
%     del_t (float): time step
%     r0 (int): row center of circular level set function
%     c0 (int): column center of circular level set function
% On output:
%     phi (MxN array): final phi array
%     tr (qx1 vector): sum(sum(abs(phi_(n+1) - phi_n)))
% Call:
%     [phi,tr] = CS6640_level_set(im,300,0.1,25,25);
% Author:
%     Kenway Sun
%     UU
%     Fall 2018
%
[M, N] = size(im);

phi = zeros(M,N);
for r=1:M
    for c=1:N
        phi(r,c) = sqrt((r-r0)^2+(c-c0)^2) - 1;
    end
end
F = CS6640_speed_fun(im);
tr = zeros(max_iter,1);
phi_pre = phi;

if del_t >= 0.71
   disp('warning: use lesser delta t')
   return;
end

for i=1:max_iter
    
    Dxp = circshift(phi, [0, -1]) - phi;
    Dxp(:, N) = 0;
    Dxn = phi - circshift(phi, [0, +1]);
    Dxn(:, 1) = 0;
    Dyp = circshift(phi, [-1, 0]) - phi;
    Dyp(M, :) = 0;
    Dyn = phi - circshift(phi, [+1, 0]);
    Dyn(1, :) = 0;
    
    del_phip = sqrt(max(Dxn, 0).^2 + min(Dxp, 0).^2 + ...
                    max(Dyn, 0).^2 + min(Dyp, 0).^2);
    del_phin = sqrt(max(Dxp, 0).^2 + min(Dxn, 0).^2 + ...
                    max(Dyp, 0).^2 + min(Dyn, 0).^2);
    
    phi = phi - del_t * (max(F,0) .* del_phip + min(F,0) .* del_phin);
    tr(i) = sum(sum(abs(phi - phi_pre)));
    phi_pre = phi;
    %combo(mat2gray(im), 1-phi);
    %pause(0.005);
end
