function F = CS6640_speed_fun(im)
    %F = 1.0 ./ (1.0 + sqrt(dx.*dx+dy.*dy).^2);
    [dx, dy] = gradient(double(im));
    mag = sqrt(dx.^2 + dy.^2);
    %F = exp(-mag);
    F = 1. ./ (1. + mag.^2);