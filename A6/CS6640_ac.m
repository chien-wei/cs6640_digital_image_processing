function im_seg = CS6640_ac(M,vidObj)
% CS6640_ac - use active contours to improve segmentation
% On input:
%     M (Matlab movie struct): segmentation movie (binary)
%     vidObj (video struct): created by readVideo
% On output:
%     im_seg (MxNxF binary array): improved segmentation provided as
%     sequence of F images
% Call:
%     vidObj = VideoReader(?../../../A4/video3.avi?);
%     ims = CS6640_ac(M,vidObj);
% Author:
%     Kenway Sun
%     UU
%     Fall 2018
%

vidObj.CurrentTime = 0;
bg = CS6640_background(vidObj);
num_frame = vidObj.Duration * vidObj.FrameRate;
im_seg = ones(vidObj.Height, vidObj.Width, num_frame);

vidObj.CurrentTime = 0;
warning('off','all');
for i=1:num_frame
    fr = mat2gray(rgb2gray(readFrame(vidObj)));
    im = fr .* mat2gray(rgb2gray(M(i).cdata));
    % frb = fr-bg;
    %[row, col, v] = find(imbinarize(im, 0.01) == 1);
    %tmp = boundary([col(:), row(:)]);
    %b1 = [col(tmp), row(tmp)];
    % plot(b(:,1), b(:,2), 'r','LineWidth',3)
    %b2 = CS6640_boundary(imbinarize(im, 0.01) == 1);
    mask = imbinarize(im, 0.01);
    mask = imdilate(mask, [[1 1 1]; [1 1 1]; [1 1 1]]);
    mask = imdilate(mask, [[1 1 1]; [1 1 1]; [1 1 1]]);
    mask = imclose(mask, [[1 1 1]; [1 1 1]; [1 1 1]]);
    mask = imopen(mask, [[1 1 1]; [1 1 1]; [1 1 1]]);
    %mask = imerode(mask, [[1 1 1]; [1 1 1]; [1 1 1]]);
    ac = activecontour(fr, mask, 10);%, 'edge');
    im_seg(:, :, i) = ac .* fr;
end
