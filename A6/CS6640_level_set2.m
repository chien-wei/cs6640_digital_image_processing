function [phi,tr] = CS6640_level_set2(im,max_iter,del_t,r0,c0)
% CS6640_level_set - level set of image
% On input:
%     im (MxN array): gray level or binary image
%     max_iter (int): maximum number of iterations
%     del_t (float): time step
%     r0 (int): row center of circular level set function
%     c0 (int): column center of circular level set function
% On output:
%     phi (MxN array): final phi array
%     tr (qx1 vector): abs(sum(sum(phi_(n+1) - phi_n)))
% Call:
%     [phi,tr] = CS6640_level_set(im,300,0.1,25,25);
% Author:
%     Tianlong Zhang
%     UU
%     Fall 2018
%
%im = mat2gray(im);
[row, col] = size(im);
phi = zeros(row,col);
%phi(r0, c0) = -2;
for r = 1: row
    for c = 1: col
        phi(r,c) = sqrt((r-r0)^2+(c-c0)^2) - 1;
    end
end
% calculate F
[Ix,Iy]=gradient(double(im));
mag=sqrt(Ix.^2+Iy.^2);
%F = exp(-mag);
F = 1. ./ (1. + mag.^2);
% make sure det_t is less than 
if del_t >= max(max(F))
    disp('input wrong')
    return;
end
tr = zeros(max_iter,1);
phi_pre = phi;
for i = 1: max_iter
    % calculate the difference
    Dx_p = circshift(phi,[0,-1]) - phi;
    Dx_p(:,col) = 0;
    Dx_n = phi - circshift(phi,[0,1]);
    Dx_n(:,1) = 0;
    Dy_p = circshift(phi,[-1,0]) - phi;
    Dy_p(row,:) = 0;
    Dy_n = phi - circshift(phi,[1,0]);
    Dy_n(1,:) = 0;
    d_phi_p = sqrt((max(Dx_n,0)).^2+(min(Dx_p,0)).^2+(max(Dy_n,0)).^2+(min(Dy_p,0)).^2);
    d_phi_n = sqrt((max(Dx_p,0)).^2+(min(Dx_n,0)).^2+(max(Dy_p,0)).^2+(min(Dy_n,0)).^2);
    phi = phi - del_t * (max(F,0).*d_phi_p + min(F,0).*d_phi_n);
    tr(i) = sum(sum(abs(phi - phi_pre)));
    phi_pre = phi;
    combo(mat2gray(im), 1-phi);
    pause(0.005)
end


