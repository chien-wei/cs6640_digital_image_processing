function [Cor, Size, Shape] = CS6640_compare_bounds(B, GT)
% CS6640_getframe - get the ist frame in video and reset video.CurrentTime
% On input:
%     B (NAx2 double): a set of pixels
%     GT (NBx2 double): a set of pixels, ground true
% On output:
%     Cor (double): ratio of true position plus false negative
%     Size (int): number of difference pixel in area
%     Shape (int): error pixels boundary
% Call:
%     [Cor, Size, Shape] = CS6640_compare_pixels(B, GT);
% Author:
%     Kenway Sun
%     UU
%     Fall 2018
%

Cor = 0;
Size = 0;
Shape = [];

% Size
area1 = polyarea(B(:, 1), B(:, 2));
area2 = polyarea(GT(:, 1), GT(:, 2));
Size = area1 - area2;

% Shape: error point in boundary
I = size(B, 1);
J = size(GT, 1);
k = 1;
for i=1:I
    flag = 0;
    for j=1:J
        if (B(i, 1) >= GT(j, 1)-1 && B(i, 1) <= GT(j, 1)+1) && ...
           (B(i, 2) >= GT(j, 2)-1 && B(i, 2) <= GT(j, 2)+1)
            flag = 1;
            break
        end
    end
    if flag == 0
        Shape(k, :) = B(i, :);
        k = k + 1;
    end
end

% Correctness area1 + area2 - a1 and a2
M = max([B(:,2);GT(:,2)]);
N = max([B(:,1);GT(:,1)]);
a1 = poly2mask(B(:, 1), B(:, 2), M, N);
a2 = poly2mask(GT(:, 1), GT(:, 2), M, N);
res = a1-a2;
Cor = size(find(res==-1),1) + size(find(res==1),1);
