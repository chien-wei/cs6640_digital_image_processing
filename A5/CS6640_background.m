function result = CS6640_background(video)
% CS6640_background - extract background image from video sequence
% On input:
%     video (video data structure): cell array of k MxNx3 images
% On output:
%     im (MxN array): (double)image
% Call:
%     im = CS6640_background(v);
% Author:
%     Kenway Sun
%     UU
%     Fall 2018
%

M = video.Height;
N = video.Width;
result = zeros(M, N);
L = video.Duration * video.FrameRate;
frames = zeros(L, M, N);

i = 0;
while hasFrame(video)
    i=i+1;
    frames(i,:,:) = rgb2gray(readFrame(video));
end

for r=1:M
    for c=1:N
        result(r,c) = median(frames(:, r, c));
    end
end

result = mat2gray(result);