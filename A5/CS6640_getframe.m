function fm = CS6640_getframe(video, i)
% CS6640_getframe - get the ist frame in video and reset video.CurrentTime
% On input:
%     video (video structure): input video
%     i: index of frame
% On output:
%     fm (MxNx3 array): output RGB image
% Call:
%     v1 = VideoReader('video1.avi');
%     fm = CS6640_getframe(video, i);
% Author:
%     Kenway Sun
%     UU
%     Fall 2018
%

if i > video.FrameRate * video.Duration
    error('Frame index out of range.')
end
video.CurrentTime = 0;
height = video.Height;
width = video.Width;
fm = zeros(height, width, 3);
j = 0;
while hasFrame(video)
    j=j+1;
    if i == j
        fm = readFrame(video);
        break;
    else
        readFrame(video);
    end
end

video.CurrentTime = 0;
