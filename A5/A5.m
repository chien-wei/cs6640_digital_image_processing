% 1 point
% [x2,y2] = getpts
% impixelinfo
% NDAS

vi = VideoReader('video.avi');
fm1 = CS6640_getframe(vi, 50);
fm1g = rgb2gray(fm1);
bg = CS6640_background(vi);
bwdiff = imabsdiff(mat2gray(fm1g), bg);
imshow(bwdiff);
load fr50.mat
plot(fr50(:,1), fr50(:,2));
x = fr50(:,1);
y = fr50(:,2);

bi = imbinarize(bwdiff, 0.05);
bi = bwareaopen(bi,15);
imshow(bi);
B = [0 1 0; 1 1 1; 0 1 0];
D = [1 0 1; 0 0 0; 1 0 1];
bi = imdilate(bi, B);
bi = imdilate(bi, D);
imshow(bi);
im2g = fm1g .* uint8(bi);
imshow(im2g);
bi2 = imerode(bi, B);
bi2 = imerode(bi2, D);
% 1 line
% flog = fspecial('log', [3,3]);
%flog = [-1 -1 -1; -1 8 -1; -1 -1 -1];
%point = imfilter(im2g, flog);

%[gv, t] = edge(im2g, 'sobel', 'vertical');
%gCannyBest = edge(im2g, 'canny', [0.04, 0.1], 1.5);
%gLoGBest = edge(im2g, 'log', 0.003, 2.25);

G = fspecial('log',[3 3],0.2);
line = imfilter(im2g, G);
line2 = line .* uint8(bi2);

imshow(line2);
imshow(line2>254);

%[row col v] = find(imbinarize(line2));
[row col v] = find(line2>254);
pre = [col(:), row(:)];
k = boundary(pre);
pre = [pre(k, 1), pre(k, 2)];

% draw two lines

hold on
plot(fr50(:,1), fr50(:,2), 'r', 'LineWidth',3);
plot(pre(:, 1),pre(:, 2), 'b', 'LineWidth',3);

[Cor, Size, Shape] = CS6640_compare_bounds(pre, fr50);

% gradient
[Gmag,Gdir] = imgradient(im2g);
imshow(mat2gray(Gmag));
imshow(mat2gray(Gmag .* bi2));

[Gmag,Gdir] = imgradient(im2g, 'sobel');
imshow(mat2gray(Gmag));
imshow(mat2gray(Gmag .* bi2)>0.3);

[Gmag,Gdir] = imgradient(im2g, 'prewitt');
imshow(mat2gray(Gmag));
imshow(mat2gray(Gmag .* bi2)>0.3);

[Gmag,Gdir] = imgradient(im2g, 'central');
imshow(mat2gray(Gmag));
imshow(mat2gray(Gmag .* bi2)>0.29);

[row col v] = find(mat2gray(Gmag .* bi2)>0.3);
pre = [col(:), row(:)];
k = boundary(pre);
pre = [pre(k, 1), pre(k, 2)];

% ostu's
imshow(imbinarize(im2g, graythresh(im2g)))

% Marr-Hildreth
gfilter= [0 0 1 0 0;
       0 1 2 1 0;
       1 2 -16 2 1;
       0 1 2 1 0;
       0 0 1 0 0];
smim = conv2(im2g, gfilter);
[rr,cc]=size(smim);
zc=zeros([rr,cc]);
for i=2:rr-1
    for j=2:cc-1
        if (smim(i,j)>0)
             if (smim(i,j+1)>=0 && smim(i,j-1)<0) || (smim(i,j+1)<0 && smim(i,j-1)>=0)
                             
                zc(i,j)= smim(i,j+1);
                        
            elseif (smim(i+1,j)>=0 && smim(i-1,j)<0) || (smim(i+1,j)<0 && smim(i-1,j)>=0)
                    zc(i,j)= smim(i,j+1);
            elseif (smim(i+1,j+1)>=0 && smim(i-1,j-1)<0) || (smim(i+1,j+1)<0 && smim(i-1,j-1)>=0)
                  zc(i,j)= smim(i,j+1);
            elseif (smim(i-1,j+1)>=0 && smim(i+1,j-1)<0) || (smim(i-1,j+1)<0 && smim(i+1,j-1)>=0)
                  zc(i,j)=smim(i,j+1);
            end
                        
        end
            
    end
end
otpt=im2uint8(zc);
otptth= otpt>105;
imshow(otpt>50);

imshow(bwdiff);
G = fspecial('log',[3 3],0.2);
line31 = imfilter(bwdiff, G);
line32 = line .* uint8(bi2);
imshow(line32);
imshow(line32>254);

[Gmag,Gdir] = imgradient(bwdiff, 'sobel');
imshow(mat2gray(Gmag));
imshow(mat2gray(Gmag .* bi2)>0.44);

[row col v] = find(mat2gray(Gmag .* bi2)>0.44);
pre = [col(:), row(:)];
k = boundary(pre);
pre = [pre(k, 1), pre(k, 2)];

plot(fr50(:,1), fr50(:,2), 'r', 'LineWidth',3);
plot(pre(:, 1),pre(:, 2), 'b', 'LineWidth',3);


% 2 hough
imgb = imbinarize(mat2gray(imread('fig1.jpg')));

[H,T,R] = hough(imgb,'RhoResolution',0.5,'Theta',-90:0.5:89);
imshow(imadjust(rescale(H)),'XData',T,'YData',R,...
'InitialMagnification','fit');
xlabel('\theta'), ylabel('\rho');
axis on, axis normal, hold on;
colormap(gca,hot);

maskH = imgb*0;
x = [1:320];
y = (rho-x*cosd(theta))/sind(theta);
for p = 1:320
    maskH(floor(y(p)),x(p)) = 1;
end

combo(mat2gray(imgb),bwmorph(maskH,'dilate',1));

% 3 graph cut
im = imread('im1.jpg');
im = rgb2gray(im);
im = im(250:300, 250:300);
[M, N] = size(im);

[C,V] = CS6640_GC(im);

combo(mat2gray(im),C==1);

im = 50 .* ones(50, 50);
im(20:40, 20:40) = 100;
[M, N] = size(im);
[C,V] = CS6640_GC(im);

combo(mat2gray(im),C==1);


im = imread('shapes.png');
im = rgb2gray(im);
im = imresize(im, 0.25);
[M, N] = size(im);
[C,V] = CS6640_GC(im);

combo(mat2gray(im),C==1);

% 4 watershed
im = -50 .* ones(50, 50);
im(20:40, 20:40) = -100;
WS = CS6640_WS(im);


im = imread('shapes.png');
im = rgb2gray(im);
im = 256 - im;
WS = CS6640_WS(im);
combo(mat2gray(im), WS)
