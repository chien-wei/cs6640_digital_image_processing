function WS = CS6640_WS(im)
% CS6640_WS - watershed (returns largest spread region)
% On input:
%     im (MxN array): input image
% On output:
%     WS (MxN array): labeled watershed regions
% Call:
%     WS = CS6640_WS(im);
% Author:
%     Kenway Sun
%     UU
%     Fall 2018

[M, N] = size(im);
% set dir
im2 = padarray(im, [1, 1], 'replicate', 'both');
dir = zeros(M, N);
for r=1:M
    for c=1:N
        a = r+1;
        b = c+1;
        F = im2(a-1:a+1, b-1:b+1);
        mi = min(F(:));
        if im(r, c) == mi
            dir(r, c) = 5;
        else
            for i=1:9
                if F(i) == mi
                    dir(r, c) = i;
                    break
                end
            end
        end
    end
end

% set the original labels as bwlabel of where dirs is 5
bw = bwlabel(dir==5);

% use find to get the unlabeled pixels.
unlabel = find(~bw);
j = size(unlabel, 1);

% go to each unlabeled pixel and set its label to that of the pixel at 
% in its dirs location
while j > 0
for i=1:j
    if i/M <= 1 || mod(i, M) == 1 || i/M > N-1 || mod(i, M) == 0
        bw(unlabel(i)) = 5;
    elseif dir(unlabel(i)) == 1
        bw(unlabel(i)) = bw(unlabel(i) - M - 1);
    elseif dir(unlabel(i)) == 2
        bw(unlabel(i)) = bw(unlabel(i) - M);
    elseif dir(unlabel(i)) == 3
        bw(unlabel(i)) = bw(unlabel(i) - M + 1);
    elseif dir(unlabel(i)) == 4
        bw(unlabel(i)) = bw(unlabel(i) - 1);
    elseif dir(unlabel(i)) == 6
        bw(unlabel(i)) = bw(unlabel(i) + 1);  
    elseif dir(unlabel(i)) == 7
        bw(unlabel(i)) = bw(unlabel(i) + M - 1);
    elseif dir(unlabel(i)) == 8
        bw(unlabel(i)) = bw(unlabel(i) + M);
    elseif dir(unlabel(i)) == 9
        bw(unlabel(i)) = bw(unlabel(i) + M + 1);
    end
end

unlabel = find(~bw);
j = size(unlabel, 1);

end

% return the deepest (largest spread, here spread means depthly spread)
WS = [];
uniq = unique(bw);
k = size(uniq, 1);
max_depth = 0;
for i=2:k
    %if size(find(WS), 1) < size(find(bw==i), 1)
    %    WS = (bw==i);
    %end
    hei = max(im(find(bw==i))) - min(im(find(bw==i)));
    if hei > max_depth
        WS = (bw==i);
        max_depth = hei;
    end
end

