function [C,V] = CS6640_GC(im)
% CS6640_GC - Graph Cut segmentation
% On input:
%     im (MxN array): input image
% On output:
%     C (MxN array): binary classification
%     V (M*Nx6 array): eigenvectors of similarity matrix
% Call:
%     [C,V] = CS6640_GC(im);
% Author:
%     Kenway Sun
%     UU
%     Fall 2018
%

[M, N] = size(im);
% compute similarity matrix W
W = zeros(M*N, M*N);

for a=1:M*N
    for b=1:M*N
        W(a, b) = exp(-abs(double(im(b)) - double(im(a))));
    end
end

[V, D] = eigs(W);
[cidx, ctrs] = kmeans(V(:,1), 2);
C = reshape(cidx, M, N);
