function [M,tracks] = CS6640_track(video)
% CS6640_track - track motion in a video
% On input:
%     video (video structure): input video
% On output:
%     M (movie structure): movie of detected differences
%     in video
%     tracks (kx2 array): row,col center of mass of largest
%     moving object in sequential video frames
% Call:
%     [M,tr] = CS6640_track(video);
% Author:
%     Kenway Sun
%     UU
%     Fall 2018
%

height = video.Height;
width = video.Width;
num_frames = video.FrameRate * video.Duration;
frames = zeros(num_frames, height, width);
i = 0;
while hasFrame(video)
    i=i+1;
    vidFrame = readFrame(video);
    frames(i,:,:) = rgb2gray(vidFrame);
end


% Get backround using CS6640_background.
video.CurrentTime = 0;
background = CS6640_background(video);
video.CurrentTime = 0;

% Get vehicle by subtract the frames to the background.
st_size = cell(1, num_frames);
M = struct('cdata', st_size, 'colormap', st_size);
tracks = zeros(num_frames, 2);

for i=1:num_frames
    img = mat2gray(reshape(frames(i, :, :), [height, width]));
    bwdiff = imabsdiff(background, img);
    im8 = uint8(255 * mat2gray(bwdiff));
    M(i).cdata = cat(3, im8, im8, im8);
    
    % Change to binary image to find center of mass.
    imb = imbinarize(im8,0.35);
    
    x = 0;
    y = 0;
    n = 0;
    for r=1:size(imb,1)
        for c=1:size(imb,2)
            if imb(r,c) == 1
                x = x + r;
                y = y + c;
                n = n + 1;
            end
        end
    end
    
    tracks(i, 1) = y/n;
    tracks(i, 2) = x/n;
end
%movie(M)
%myVideo = VideoWriter('track.avi');
%open(myVideo);
%writeVideo(myVideo,M)
%close(myVideo);