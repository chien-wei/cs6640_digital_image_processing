function b = CS6640_boundary(im)
% CS6640_boundary - finds boundary pixels of single object
% On input:
%     im (MxN binary image): single connected component image
% On output:
%     b (kx2 array): row, column values of boundary pixels (i.e.,
%     column 1 of b is rows; column 2 of b is cols
%     unless no object, in which case return []
% Call:
%     b = CS6640_boundary(im);
% Author:
%     Kenway Sun
%     UU
%     Fall 2018
ime = bwmorph(im,'erode',1);
imd = im - ime;
[rows,cols] = find(imd);
b = [rows,cols];