function s = CS6640_object_size(MM)
% CS6640_object_size - get size for frames in movie object
% On input:
%     M (Matlab movie): movie of segmented moving objects
%     vidObj (video object obtained by VideoReader): input video
% On output:
%     s (nx1 array): n samples of size vectors
% Call:
%     CS6640_object_size(MM);
% Author:
%     Kenway Sun
%     Fall 2018
%     UU
%
L = size(MM, 2);
s = zeros(L,1);
for i=1:L
    s(i) = size(find(rgb2gray(MM(i).cdata)),1);
end