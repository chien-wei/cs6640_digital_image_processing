camera = webcam; % requires installing the webcam Add-On support
nnet = alexnet; % requires installing the alexnet Add-on support
picture = camera.snapshot;
picture = imresize(picture,[227,227]);
image(picture);
label = classify(nnet, picture);
title(char(label));
drawnow
clear camera


X1 = linspace(-5,5);
y1 = abs(sinc(X1)) + 2*cos(X1)/10;
plot(X1,y1,'o')
grid

net = feedforwardnet(100);
net = configure(net,X1,y1);
t1 = net(X1)
plot(X1,y1,'o', X1,t1,'x')

net.trainParam.epochs = 10;
net = train(net,X1,y1);
t2 = net(X1)
plot(X1,y1,'o',X1,t2,'x')


% tom's code not usable
net = feedforwardnet([5]);
net.trainParam.epochs = 1000;
[net,tr,Y,E] = train(P,T);
indexes = randperm(15);
X2 = P(indexes);
aa = net(X2);
pc = 100*sum(round(aa)==T(indexes)/15;


s_b1 = CS6640_object_size(M_b1);
s_b2 = CS6640_object_size(M_b2);
s_b3 = CS6640_object_size(M_b3);

s_c1 = CS6640_object_size(M_c1);
s_c2 = CS6640_object_size(M_c2);
s_c3 = CS6640_object_size(M_c3);

s_t1 = CS6640_object_size(M_t1);
s_t2 = CS6640_object_size(M_t2);
s_t3 = CS6640_object_size(M_t3);

% c = 1, b = 2, t = 3

X = [[s_c1]' [s_c2]' [s_t1]' [s_t2]' [s_b1]' [s_b2]'];
Y = [ones(1, 130) ones(1, 192) ...
     ones(1, 130)*2 ones(1, 167)*2....
     ones(1, 200)*3 ones(1, 200)*3.... 
     ];

net = feedforwardnet(100);
net.trainParam.epochs = 1000;
net = train(net,X,Y);
T1 = net(X);
plot(X,Y,'o',X,T1,'x');


% confusion table
table = zeros(3);
for i=1:size(T1, 2)
    x = Y(i);
    y = max(1, min(3, round(T1(i))));
    table(x, y) = table(x, y) + 1;
end
table
% accuracy
table(1, 1) / sum(table(1, :))
table(2, 2) / sum(table(2, :))
table(3, 3) / sum(table(3, :))


X2 = [[s_c3]' [s_t3]' [s_b3]'];
Y2 = [ones(1, 95) ...
     ones(1, 192)*2 ...
     ones(1, 255)*3 ...
     ];
T2 = net(X2);

table2 = zeros(3);
for i=1:size(T2, 2)
    x = Y2(i);
    y = max(1, min(3, round(T2(i))));
    table2(x, y) = table2(x, y) + 1;
end
table2
% accuracy
table2(1, 1) / sum(table2(1, :))
table2(2, 2) / sum(table2(2, :))
table2(3, 3) / sum(table2(3, :))


% now use more feature
s_b1 = CS6640_object_features(M_b1);
s_b2 = CS6640_object_features(M_b2);
s_b3 = CS6640_object_features(M_b3);

s_c1 = CS6640_object_features(M_c1);
s_c2 = CS6640_object_features(M_c2);
s_c3 = CS6640_object_features(M_c3);

s_t1 = CS6640_object_features(M_t1);
s_t2 = CS6640_object_features(M_t2);
s_t3 = CS6640_object_features(M_t3);


X = [[s_c1]' [s_c2]' [s_t1]' [s_t2]' [s_b1]' [s_b2]'];
Y = [ones(1, 130) ones(1, 192) ...
     ones(1, 130)*2 ones(1, 167)*2 ...
     ones(1, 200)*3 ones(1, 200)*3 ... 
     ];
 
net = feedforwardnet(100);
net.trainParam.epochs = 100;
net = train(net,X,Y);
T1 = net(X);
plot(X,Y,'o',X,T1,'x');

% confusion table
table = zeros(3);
for i=1:size(T1, 2)
    x = Y(i);
    y = max(1, min(3, round(T1(i))));
    table(x, y) = table(x, y) + 1;
end
table
% accuracy
table(1, 1) / sum(table(1, :))
table(2, 2) / sum(table(2, :))
table(3, 3) / sum(table(3, :))


X2 = [[s_c3]' [s_t3]' [s_b3]'];
Y2 = [ones(1, 95) ...
     ones(1, 192)*2 ...
     ones(1, 255)*3 ...
     ];
T2 = net(X2);

table2 = zeros(3);
for i=1:size(T2, 2)
    x = Y2(i);
    y = max(1, min(3, round(T2(i))));
    table2(x, y) = table2(x, y) + 1;
end
table2
% accuracy
table2(1, 1) / sum(table2(1, :))
table2(2, 2) / sum(table2(2, :))
table2(3, 3) / sum(table2(3, :))


% current best
net = feedforwardnet(100);
net.trainParam.epochs = 10;
net = trainoss(net,X,Y);
T1 = net(X);
plot(X,Y,'o',X,T1,'x');
% confusion table
table = zeros(3);
for i=1:size(T1, 2)
x = Y(i);
y = max(1, min(3, round(T1(i))));
table(x, y) = table(x, y) + 1;
end
table
% accuracy
table(1, 1) / sum(table(1, :))
table(2, 2) / sum(table(2, :))
table(3, 3) / sum(table(3, :))
X2 = [[s_c3]' [s_t3]' [s_b3]'];
Y2 = [ones(1, 95) ...
ones(1, 192)*2 ...
ones(1, 255)*3 ...
];
T2 = net(X2);
table2 = zeros(3);
for i=1:size(T2, 2)
x = Y2(i);
y = max(1, min(3, round(T2(i))));
table2(x, y) = table2(x, y) + 1;
end
table2
% accuracy
table2(1, 1) / sum(table2(1, :))
table2(2, 2) / sum(table2(2, :))
table2(3, 3) / sum(table2(3, :))