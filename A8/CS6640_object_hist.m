function hist = CS6640_object_hist(video, MM)
% CS6640_object_hist - get hist for movie using CS6640_hist_by_pixels
% On input:
%     M (Matlab movie): movie of segmented moving objects
%     vidObj (video object obtained by VideoReader): input video
%     video (Matlab video object): original video that create M
% On output:
%     hist (nxm array): n samples of m-tuple feature vectors
% Call:
%     hist = CS6640_object_hist(video, MM)
% Author:
%     Kenway Sun
%     Fall 2018
%     UU
%

L = size(MM, 2);
hist = zeros(L, 8);
i = 0;
while hasFrame(video)
    i = i + 1;
    vidFrame = rgb2gray(readFrame(video));
    pixels = find(rgb2gray(MM(i).cdata));
    hist(i, :) = CS6640_hist_by_pixels(pixels, vidFrame, 8);
end
video.CurrentTime = 0;