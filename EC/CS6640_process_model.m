function mu_t = CS6640_process_model(mu_tm1,A,B,u,R)
% CS6640_process_model - determine next state
% On input:
%     mu_tm1 (4x1 vector): state at t-1
%     A (4x4 array): linear process
%     B (4x4 array): control process
%     u (4x1 vector): control vector
%     R (4x4 array): process covariance
% On output:
%     mu_t (4x1 vector): state at time t
% Call:
%     x = Cs6640_process_model(x,A,B,u,R);
% Author:
%     T. Henderson
%     UU
%     Fall 2018
%

L = chol(R,'lower');
n = length(mu_tm1);
x = randn(n,1);
mu_t = A*mu_tm1 + B*u + L*x;
