function z = CS6640_sensor(x,C,Q)
% CS6640_sensor - return sensed state values
% On input:
%     x (4x1 vector): actual state
%     C (2x4 array): sensor process
%     Q (2x2 array): sensor covariance
% On output:
%     z (2x1 vector): x,y sensed values
% Call:
%     z = Cs6640_sensor([1;1000;0;2],eye(2,4),[0.1,0;0,0.1]);
% Author:
%     T. Henderson
%     UU
%     Fall 2018
%

L = chol(Q,'lower');
[k,n] = size(C);
z = C*x + L*randn(k,1);
