function [xt,vt] = CS6640_sim(a,x0,v0,del_t,sigma2,max_t)
% CS6640_sim - speed function for horizontal step edges
% On input:
%     a (float): acceleration
%     x0 (float): initial position
%     v0 (float): initial velocity
%     del_t (float): time step
%     sigma2 (float): variance for x location
%     max_t (float): max time for simulation
% On output:
%     xt (qx1 vector): trace od position over time
%     vt (qx1 vector): trace of velocity over time
% Call:
%     [xt,vt] = CS6640_sim(-9.8,0,100,0.1,1,3);
% Author:
%     T. Henderson
%     UU
%     Fall 2018

x = x0;
v = v0;
xt = [x];
vt = [v];
t = 0;

while t<max_t
    v = v + a*del_t;
    x = x + v*del_t + sqrt(sigma2)*randn;
    xt = [xt;x];
    vt = [vt;v];
    t = t + del_t;
end