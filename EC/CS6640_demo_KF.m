function tr = CS6640_demo_KF(del_t,max_time)
% CS6640_demo_KF - demo the KF function
% On input:
%     del_t (float): time step
%     max_time (float): max time to simulate
% On output:
%     tr (nx10 array): trace; each row is:
%        actual (x,y,vx,vy), KF estimate (x,y,vx,vy), sensor (x,y)
% Call:
%     tr = CS6640_demo_kf(0.1,5);
% Author:
%     T. Henderson
%     UU
%     Fall 2018
%

mu_0 = [1;1000;0;0];
Sigma_0 = 0.001*eye(4);
u = [0;0;0;-9.8];
B = [0 0 0 0; 0 0 0 0; 0 0 0 0; 0 0 0 del_t];
A = [1 0 del_t 0; 0 1 0 del_t; 0 0 1 0; 0 0 0 1];
C = eye(2,4);
R = 0.5*eye(4);
Q = 2*eye(2);
max_iter = ceil(max_time/del_t);
tr = CS6640_run_KF(mu_0,Sigma_0,u,A,B,C,R,Q,max_iter);

figure(1);
clf
plot([1:length(tr(:,1))],tr(:,2),'k.');
hold on
plot([1:length(tr(:,1))],tr(:,6),'ro');
plot([1:length(tr(:,1))],tr(:,10),'g+');
xlabel('Time Step');
ylabel('Height');
legend('Actual','Kalman Estimate','Sensor');
