del_t = 1;
A_t = [1    , 0    , del_t , 0     ; ...
       0    , 1    , 0     , del_t ; ...
       0    , 0    , 1     , 0     ; ...
       0    , 0    , 0     , 1     ];
B_t = zeros(4);
C_t = eye(4);
Q_t = orth(randn(4));%eye(4) * 0.1;
R_t = eye(4) * 0.1;
u_t = zeros(4, 1);

[mu_t,Sigma_t] = CS6640_KF(mu_tm1,Sigma_tm1,u_t,z_t,A_t,R_t,B_t,C_t,Q_t)


% set up
im = uint8(zeros(480, 640));
pos = zeros(20, 2);
for i=1:20
    pos(i, :) = [10+20*i, 10+30*i];
end

imshow(im)
hold on
plot(640 - pos(:, 2), pos(:, 1))


x = 640 - pos(2, 2);
y = pos(2, 1);
z = [x, y, 640 - pos(1, 2) - x, pos(1, 1) - y];
z_t = z';
mu_tm1 = [x; y; 0; 0];
Sigma_tm1 = cov(mu_tm1);
[mu_t,Sigma_t] = CS6640_KF(mu_tm1,Sigma_tm1,u_t,z_t,A_t,R_t,...
                 B_t,C_t,Q_t)
for i=3:20
    x = 640 - pos(i, 2);
    y = pos(i, 1);
    z = [x, y, 640 - pos(i-1, 2) - x, pos(i-1, 1) - y];
    z;
    z_t = z';
    
    [mu_t,Sigma_t] = CS6640_KF(mu_t,Sigma_t,u_t,z_t,A_t,R_t,...
                     B_t,C_t,Q_t);
    imshow(im)
    hold on
    plot(640 - pos(:, 2), pos(:, 1), 'b')
    plot(mu_t(1), mu_t(2), 'ro')
    pause(0.1)
end


[xt,vt] = CS6640_sim(-9.8,0,100,0.1,1,3)