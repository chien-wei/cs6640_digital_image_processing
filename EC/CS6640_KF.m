function [mu_t,Sigma_t] = CS6640_KF(mu_tm1,Sigma_tm1,u_t,z_t,A_t,R_t,...
B_t,C_t,Q_t)
% CS6640_KF - one step in Kalman Filter
% On input:
%     mu_tm1 (nx1 vector): previous state estimate vector
%     Sigma_tm1 (nxn matrix): state covariance matrix
%     u_t (mx1 vector): control vector
%     z_t (kx1 vector): measurement vector
%     A_t (nxn matrix): state transition matrix
%     R_t (nxn matrix): state transition covariance matrix
%     B_t (nxm matrix): control matrix for proces equation
%     C_t (kxn matrix): linear transform for measurement equation
%     Q_t (kxk matrix): sensor noise covariance matrix
% On output:
%     mu_t (nx1 vector): next state estimate
%     Sigma_t (nxn matrix): state covariance matrix
% Call:
%     [x,Sigma2] = CS6640_KF(x,Sigma2,u,z,A,R,B,C,Q);
% Author:
%     Kenway
%     UU
%     Fall 2018
%

N = size(Sigma_tm1);

mu_t = A_t * mu_tm1 + B_t * u_t;
Sigma_t = A_t * Sigma_tm1 * A_t' + R_t;
k_t = Sigma_t * C_t' * inv(C_t * Sigma_t * C_t' + Q_t);
mu_t = mu_t + k_t * (z_t - C_t * mu_t);
Sigma_t = (eye(N) - k_t * C_t) * Sigma_t;



