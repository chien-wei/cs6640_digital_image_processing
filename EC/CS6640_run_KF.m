function trace = CS6640_run_KF(mu_0,Sigma_0,u,A,B,C,R,Q,max_iter)
% CS6640_run_KF - run the KF function
% On input:
%     mu_0 (4x1 vector): initial x,y,vx,vy
%     Sigma_0 (4x4 array): initial covariance
%     u (4x1 vector): control vector (for gravity in this case)
%     A (4x4 array): linear process
%     B (4x4 array): control process
%     C (2x4 array): sensor process
%     R (4x4 array): process covariance
%     Q (2x2 array): sensor covariance
%     max_iter (int): max number of step
% On output:
%     trace (nx10 array): trace; each row is:
%        actual (x,y,vx,vy), KF estimate (x,y,vx,vy), sensor (x,y)
% Call:
%     tr = CS6640_run_KF(mu_0,Sigma_0,u,A,B,C,R,Q,max_iter);
% Author:
%     T. Henderson
%     UU
%     Fall 2018
%

z0 = CS6640_sensor(mu_0,C,Q);
trace = [mu_0',mu_0',z0'];
mu = mu_0;
Sigma = Sigma_0;
x = mu_0;
iter = 0;

while iter<max_iter
    iter = iter + 1;
    x = CS6640_process_model(x,A,B,u,R);
    x(1) = mu_0(1);
    z = CS6640_sensor(x,C,Q);
    [mu,Sigma] = CS6640_KF(mu,Sigma,u,z,A,R,B,C,Q);
    trace = [trace;x',mu',z'];
end
