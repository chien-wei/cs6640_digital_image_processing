function result = CS6640_background_hue(video)
% CS6640_background_heq - extract background image from video sequence
% histogram equalization version
% On input:
% video (video data structure): cell array of k MxNx3 images
% On output:
% im (MxN array): (double)image
% Call:
% im = CS6640_background(v);
% Author:
% Kenway Sun
% UU
% Fall 2018
%

M = video.Height;
N = video.Width;
result = zeros(M, N);
L = video.Duration * video.FrameRate;
% need to translate to int to show the histeq(im) correct.
frames = zeros(L, M, N);

i = 0;
while hasFrame(video)
    i=i+1;
    frame = readFrame(video);
    frame = rgb2hsv(frame);
    frames(i,:,:) = reshape(frame(:,:,1), [M, N]);
end

for r=1:M
    for c=1:N
        result(r,c) = median(frames(:,r,c));
    end
end

