function N = add_numbers(object_data, M)
% add_numbers - adding numbers in object_data to M and get another 
% movie with all marks on it.
% On input:
%     object_data: structure. Result from CS6640_object_data.
%     M (Matlab movie): Result from CS6640_MM.
% On output:
%     N (Matlab movie): movie of marked segmented moving objects
% Call:
%     vidObj = VideoReader('../../../video.avi');
%     M = CS6640_MM(vidObj);
%     vidObj.CurrentTime = 0;
%     object_data = CS6640_object_data(M, vidObj);
%     N = add_numbers(object_data, M);
% Author:
%     Kenway Sun
%     Fall 2018
%     UU
%

%vidObj = VideoReader('video1.avi');
%M = CS6640_MM(vidObj);
%vidObj.CurrentTime = 0;
%object_data = CS6640_object_data(M, vidObj);


[z, L] = size(M);
N(L) = struct('cdata',[], 'colormap', []);

for i=1:L
    imshow(M(i).cdata)
    hold on
    
    num_obj = object_data(i).num_objects;
    for j = 1:num_obj
        o = object_data(i).objects(j);
        bbulr = o.ul_row;
        bbulc = o.ul_col;
        bbrd = o.lr_row - o.ul_row;
        bbcd = o.lr_col - o.ul_col;
        
        r = o.row_mean;
        c = o.col_mean;
        
        %rectangle('Position',[bbulr,bbulc,bbrd,bbcd], ...
        %          'EdgeColor','b',...
        %          'LineWidth',2)
        text(r+20, c+20, int2str(o.red_median), 'Color', 'red');
        text(r+20, c, int2str(o.green_median), 'Color', 'green');
        text(r+20, c-20, int2str(o.blue_median), 'Color', 'blue');
        text(r+20, c-40, int2str(o.num_pixels), 'Color', 'yellow');
    end
          
    N(i) = getframe;
    clf;
end
