function run_A4()
% Matlab functions I tried. Execute A4.
% On input:
%     None
% On output:
%     None
% Call:
%     run_A4()
% Author:
%     Kenway Sun
%     Fall 2018
%     UU
%

v3 = VideoReader('video3.avi');
video = v3;
% get background
background = CS6640_background(video);

im = imread('original.jpg');
img = rgb2gray(im);

% difference between img and background
dif = imabsdiff(background, mat2gray(img));
imshow(dif);

% binary image of dif
imshow(imbinarize(dif, 0.05));
imshow(imbinarize(dif, 'global'));
imshow(imbinarize(dif, 'adaptive'));
imshow(imbinarize(dif,'adaptive','ForegroundPolarity','dark', ...
                  'Sensitivity', 1.0));
bi = imbinarize(dif, 0.17);
imshow(bi);

% imdilate
B = [0 1 0; 1 1 1; 0 1 0];
bid = imdilate(bi, B);
imshow(bid);

bidd = imdilate(bid, B);
imshow(bidd);

biddd = imdilate(bidd, B);
imshow(biddd);

biddde = imerode(biddd, B);
imshow(biddde);

combo(mat2gray(img), biddde);

% cut car
im = imread('cut2.jpg');
img = rgb2gray(im);

% difference between img and background
dif = imabsdiff(background, mat2gray(img));
imshow(dif);

% binary image of dif
bi = imbinarize(dif, 0.17);
imshow(bi);

% imdilate
B = [0 1 0; 1 1 1; 0 1 0];
bid = imdilate(bi, B);
imshow(bid);

bidd = imdilate(bid, B);
imshow(bidd);

C = [0 0 1; 1 0 1; 1 0 0];
biddc = imdilate(bidd, C);
imshow(biddc);

biddd = imdilate(bidd, B);
imshow(biddd);

biddde = imerode(biddd, B);
imshow(biddde);

combo(mat2gray(img), biddde);
imshow(mat2gray(rgb2gray(vidFrame)) .* biddde);

BW = imbinarize(rgb2gray(M3(55).cdata), 0.5);
CC = regionprops(BW,'centroid', 'BoundingBox', 'Area', 'PixelIdxList');
