function CS6640_week9
%
% run in CS6640/KB

L3 = fspecial('log',3,1)
L5 = fspecial('log',5,2)
L7 = fspecial('log',7,2)
A = zeros(21,21);
A(11,:) = 1;
figure(2);
clf
surf(A);

A3 = imfilter(A,L3);
A5 = imfilter(A,L5);
figure(1);
clf
surf(A3);
surf(A5);

A(10,:) = 1;
A(12,:) = 1;

A3 = imfilter(A,L3);
A5 = imfilter(A,L5);
figure(1);
clf
surf(A3);
surf(A5);

% Laplacian Line/Edge Detection
vidObj = VideoReader('video1.avi');
[M,tr] = CS6640_track(vidObj);

im1 = rgb2gray(M(25).cdata);
im1o = im1;
%im1 = im1(30:509,87:726);
im1L = imfilter(im1,L7);
imshow(mat2gray(im1L));
imshow(mat2gray(im1L>4));

[dx,dy] = gradient(double(im1));
mag = sqrt(dx.^2+dy.^2);
imshow(mag>25);
d = imfilter(mag,ones(30,80)/(30*80));
imshow(mat2gray(d));
surf(d);

im1e = edge(im1,'canny',[0.1,0.3],2);
imshow(im1e);

% Hough
mask = zeros(480,640);
for p = 1:length(tr(:,1))
    mask(tr(p,1),tr(p,2)) = 1;
end
combo(mat2gray(im1),bwmorph(mask,'dilate',2));

[H,T,R] = hough(mask,'rhoResolution',0.5,'Theta',-90:0.5:89);
[rho_max,theta_max] = find(H==max(max(H)));
rho = R(rho_max)
theta = T(theta_max)
maskH = mask*0;
x = [1:640];
y = (rho-x*cosd(theta))/sind(theta);
for p = 1:640
    maskH(floor(y(p)),x(p)) = 1;
end
clf
plot(tr(:,2),641-tr(:,1),'ro');
hold on
axis equal
plot(x,641-y,'k.');
clf

combo(mat2gray(im1),bwmorph(maskH,'dilate',2));

% PCA Approach
x = tr(:,2);
y = 641 - tr(:,1);
pts = [x,y];
xm = mean(x);
ym = mean(y);
pts0(:,1) = pts(:,1) - xm;
pts0(:,2) = pts(:,2) - ym;
C = pts0'*pts0;
[V,D] = eigs(C);
dir = [V(1,1),V(2,1)];
da = [-558:425];
clf
plot(x,y,'ro');
hold on
axis equal
plot(xm,ym,'b*');
ptsp1 = xm + da*dir(1);
ptsp2 = ym + da*dir(2);
ptsp = [ptsp1',ptsp2'];
plot(ptsp(:,1),ptsp(:,2),'g.');

% Thresholding
clf
hist(double(im1o(:)));  % bi-modal?  Pick value between modes
combo(mat2gray(im1),~(bwmorph(im1>100,'dilate',6)));

% SLIC
[L,NumLabels] = superpixels(d,40);
BW = boundarymask(L);
imshow(imoverlay(im1,BW,'red'));

% Graph Cuts
d2 = imresize(d,[50,50]);
C = CS6640_GC(d2);
imshow(mat2gray(C==1));
combo(mat2gray(d2),~(C==1));
surf(C);

tch = 0;
