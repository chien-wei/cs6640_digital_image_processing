function M = CS6640_MM2(vidObj)
% CS6640_MM - segments moving objects in video
% On input:
%     vidObj (video object obtained by VideoReader): input video
% On output:
%     M (Matlab movie): movie of segmented moving objects
% Call:
%     vidObj = VideoReader('../../../video.avi');
%     M = CS6640_MM(vidObj);
% Author:
%     Kenway Sun
%     Fall 2018
%     UU
%

%v2 = VideoReader('video2.avi');
%vidObj = v2;

H = vidObj.Height;
W = vidObj.Width;
background = CS6640_background(vidObj);
vidObj.CurrentTime = 0;

L = vidObj.Duration * vidObj.FrameRate;
frames = zeros(L, H, W);
i = 0;
M(L) = struct('cdata',[],'colormap',[]);

B = [0 1 0; 1 1 1; 0 1 0];
F = [0 1 0; 1 0 1; 0 1 0];
D = [1 0 1; 0 0 0; 1 0 1];
R = [1 1 1; 1 1 1; 1 1 1];
while hasFrame(vidObj)
    i=i+1;
    vidFrame = readFrame(vidObj);
    dif = imabsdiff(mat2gray(rgb2gray(vidFrame)), background);
    bi = imbinarize(dif, 0.17);
    bid = imdilate(bi, B);
    bid = imdilate(bid, B);
    bid = imdilate(bid, B);
    bid = imdilate(bid, F);
    bid = imdilate(bid, D);
    bid = imdilate(bid, F);
    bid = imdilate(bid, D);
    bid = imdilate(bid, F);
    bid = imdilate(bid, D);
    bid = imdilate(bid, F);
    bid = imdilate(bid, D);
    bid = imdilate(bid, F);
    bid = imdilate(bid, D);
    bid = imdilate(bid, F);
    bid = imdilate(bid, D);
    bid = imerode(bid, R);
    bid = imerode(bid, R);
    bid = imerode(bid, B);
    bid = imerode(bid, B);
    bid = imerode(bid, R);
    bid = imerode(bid, R);
    bid = imerode(bid, B);
    bid = imerode(bid, B);
    bid = imerode(bid, R);
    bid = imerode(bid, R);
    bid = imerode(bid, B);
    bid = imerode(bid, B);
    imshow(bid);
    %imshow(mat2gray(rgb2gray(vidFrame)) .* bid);
    M(i) = getframe;
    %frames(i,:,:) = rgb2gray(vidFrame);
    %imshow(mat2gray(reshape(frames(i, :, :), [M, N])));
end

