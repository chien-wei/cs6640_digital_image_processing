function M = CS6640_MM_gl(vidObj)
% CS6640_MM - segments moving objects in video
% On input:
%     vidObj (video object obtained by VideoReader): input video
% On output:
%     M (Matlab movie): movie of segmented moving objects
% Call:
%     vidObj = VideoReader('../../../video.avi');
%     M = CS6640_MM(vidObj);
% Author:
%     Kenway Sun
%     Fall 2018
%     UU
%

%vidObj = VideoReader('video1.avi');


H = vidObj.Height;
W = vidObj.Width;
background = CS6640_background(vidObj);
vidObj.CurrentTime = 0;

L = vidObj.Duration * vidObj.FrameRate;
frames = zeros(L, H, W);
% Set background as last_fr so it won't look strange.
last_fr = background;
i = 0;
M(L) = struct('cdata',[], 'colormap', []);

B = [0 1 0; 1 1 1; 0 1 0];
D = [1 0 1; 0 0 0; 1 0 1];
R = [1 1 1; 1 1 1; 1 1 1];

while hasFrame(vidObj)
    i=i+1;
    vidFrame = readFrame(vidObj);
    bk_dif = imabsdiff(mat2gray(rgb2gray(vidFrame)), background);
    fr_dif = imabsdiff(mat2gray(rgb2gray(vidFrame)), last_fr);

    sum_dif = bk_dif + fr_dif;
    % need a threshold
    bi = imbinarize(sum_dif, 0.2);
    bi = bwareaopen(bi,15);
    
    bi = imdilate(bi, B);
    bi = imdilate(bi, D);
    bi = imdilate(bi, B);
    bi = imdilate(bi, D);
    bi = imdilate(bi, B);
    bi = imerode(bi, R);
    bi = imerode(bi, R);
    
    se = strel('rectangle', [9 9]);
    bi = imclose(bi, se);
    
    %imshow(bi);
    imshow(mat2gray(rgb2gray(vidFrame)) .* bi);
    M(i) = getframe;
    last_fr = mat2gray(rgb2gray(vidFrame));
    %frames(i,:,:) = rgb2gray(vidFrame);
    %imshow(mat2gray(reshape(frames(i, :, :), [M, N])));
end

