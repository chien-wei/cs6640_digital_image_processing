function g = CS6640_corr2(f,w)
% CS6640_correl2D - 2D cross correlation
% On input:
% f (MxN float array): input image
% w ((2a+1)x(2b+1) array): filter
% On output:
% g (MxN float array): cross correlation of f with w
% Call:
% g = CS6640_correl2d(f,w);
% Author:
% <Your Name>
% UU
% Fall 2018
%

[M,N] = size(f);
[k1,k2] = size(w);
a = (k1-1)/2;
b = (k2-1)/2;
g = zeros(M,N);
for r = a+1:M-a
    for c = b+1:N-b
        fw = f(r-a:r+a,c-b:c+b);
        g(r,c) = dot(double(fw(:)),double(w(:)));
    end
end