function CS6640_differential2(img, Dx, name)

Dy = Dx';
dx = imfilter(img,Dx);
dy = imfilter(img,Dy);

dx = double(dx)
dy = double(dy)
mag = sqrt(dx.^2+dy.^2);
ori = posori(atan2(dy,dx));
imshowpair(mag,ori,'montage')
saveas(gcf,strcat(name, '_montage3.png'))



