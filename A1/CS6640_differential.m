function res = CS6640_differential(img, Dx, name)

Dy = Dx';
dx = imfilter(img,Dx);
dy = imfilter(img,Dy);
tmp = bwmorph(dy,'dilate',3);
imwrite(tmp(160:280, 260:500), strcat(name, '_dy_dilate_3.png'));
tmp = bwmorph(dy,'dilate',1);
imwrite(tmp(160:280, 260:500), strcat(name, '_dy_dilate_1.png'));
tmp = bwmorph(dy,'erode',1);
imwrite(tmp(160:280, 260:500), strcat(name, '_dy_erode_1.png'));
tmp = bwmorph(dy,'erode',2);
imwrite(tmp(160:280, 260:500), strcat(name, '_dy_erode_2.png'));
tmp = bwmorph(dy,'close',1);
imwrite(tmp(160:280, 260:500), strcat(name, '_dy_close_1.png'));
tmp = bwmorph(dy,'skel',1);
imwrite(tmp(160:280, 260:500), strcat(name, '_dy_skel_1.png'));
tmp = bwmorph(dy,'skel',10);
imwrite(tmp(160:280, 260:500), strcat(name, '_dy_skel_10.png'));
tmp = bwmorph(dy,'shrink',1);
imwrite(tmp(160:280, 260:500), strcat(name, '_dy_shrink_1.png'));
tmp = bwmorph(dy,'shrink',3);
imwrite(tmp(160:280, 260:500), strcat(name, '_dy_shrink_3.png'));

tmp = bwmorph(dx,'dilate',1);
imwrite(tmp(160:280, 260:500), strcat(name, '_dx_dilate_1.png'));

