function cell_array = prepare_background()
% prepare_background - to run my CS6640_background
% On input:
% None
% On output:
% 1xk cell array
% Call:
% cell_array = prepare_background()
% Author:
% Chien-Wei
% UU
% Fall 2018
%

PATH_FOLDER = '08-22-18/08-22-18_09-20-34-780';

jpgs = dir(strcat(PATH_FOLDER, '/*.jpg'));
cell_array = cell(1, length(jpgs));
for i=1:length(jpgs)
cell_array{i} = imread(strcat(PATH_FOLDER, '/', jpgs(i).name));
end
