function result = CS6640_background(video)
% CS6640_background - extract background image from video sequence
% On input:
% video (video data structure): cell array of k MxNx3 images
% On output:
% im (MxN array): image
% Call:
% im = CS6640_background(v);
% Author:
% Chien-Wei
% UU
% Fall 2018
%

NUM_FRAMES = size(video);
NUM_FRAMES = NUM_FRAMES(2);

result = double(video{1})/NUM_FRAMES;

for i=2:NUM_FRAMES
result = result + double(video{i})/NUM_FRAMES;
end
result = uint8(result);


