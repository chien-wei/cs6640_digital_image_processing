function res = CS6640_median2D(f)

[M, N] = size(f);

a = 1;
b = 1;
for r = a+1:M-a
    for c = b+1:N-b
        fw = f(r-a:r+a, c-b:c+b);
        res(r, c) = median(fw(:));
    end
end
