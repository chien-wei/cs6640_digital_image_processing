function run_register(img1)
% run_register - code to play with CS6640_register
% On input:
%     img1 (MxN array): input image
% On output:
%     None, but it will render results on figure 1, 2, 3
% Call:
%     run_register(img1);
% Author:
%     Chien-Wei
%     UU
%     Fall 2018
%

num_cor = 12;
[M,N] = size(img1);
indexes1 = sort(max(1,floor(M*N*rand(1,num_cor))));
indexes2 = sort(max(1,floor(M*N*rand(1,num_cor))));
cpts = zeros(2*num_cor, 2);
for i=1:num_cor
    k = indexes1(i);
    cpts(2*i - 1, :) = [floor(k/N), mod(k,N)];
    %cpts(2*i, :) = [floor(k/N/1.3)-50, uint16(mod(k,N)*1.2)-30];
    %cpts(2*i, :) = [floor(k/N*1.8)-50, uint16(mod(k,N)*1.8)];
    k = indexes2(i);
    %cpts(2*i, :) = [floor(k/N*1.8)-50, uint16(mod(k,N)*1.8)];
    cpts(2*i, :) = [floor(k/N), mod(k,N)];
end

figure(1)
clf
imshow(img1)
hold
% order of ploting is reverse...
for i=1:num_cor
    plot(cpts(2*i - 1, 2), cpts(2*i - 1, 1), 'ro')
    plot(cpts(2*i, 2), cpts(2*i, 1), 'go')
end

figure(2)
clf
s = 0;
[imr,q,A] = CS6640_register(img1, s, cpts);
imshow(imr);

figure(3)
clf
s = 1;
[imr,q,A] = CS6640_register(img1, s, cpts);
imshow(imr);