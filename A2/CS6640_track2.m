function [M,tracks] = CS6640_track2(video)
% CS6640_track - track motion in a video
% On input:
% video (video structure): input video
% On output:
% M (movie structure): movie of detected differences
% in video
% tracks (kx2 array): row,col center of mass of largest
% moving object in sequential video frames
% Call:
% [M,tr] = CS6640_track(video);
% Author:
% Chien-Wei Sun
% UU
% Fall 2018
%

vidObj = VideoReader(video);

num_frames = vidObj.NumberOfFrames;
frames = zeros(num_frames, vidObj.Height, vidObj.Width);
vidObj = VideoReader(video);
i = 0;

% Store frames.
while hasFrame(vidObj)
    i = i+1;
    vidFrame = readFrame(vidObj);
    frames(i,:,:) = rgb2gray(vidFrame);
    %imshow(vidFrame);
    %input('Hit Return')
end

% Get backround first.
background = zeros(vidObj.Height, vidObj.Width);

for r=1:vidObj.Height
    for c=1:vidObj.Width
        background(r,c) = mean(frames(:, r, c));
    end
end

background = mat2gray(background);

% Get vehicle by subtraction of the frames to the background.
i = 0;
vidObj = VideoReader(video);
M(num_frames) = struct('cdata',[],'colormap',[]);
tracks = zeros(num_frames, 2);
while hasFrame(vidObj)
    i = i + 1;
    vidFrame = readFrame(vidObj);
    img = im2uint8(mat2gray(rgb2gray(vidFrame)));
    dif = imabsdiff(im2uint8(background), img);
    %dif = imsharpen(dif);
    %level = graythresh(dif);
    imshow(dif);
    dif = imbinarize(dif,0.25);
    %imshow(dif);
    
    
    x = 0;
    y = 0;
    n = 0;
    for r=1:size(dif,1)
        for c=1:size(dif,2)
            if dif(r,c) == 1
                x = x + r;
                y = y + c;
                n = n + 1;
            end
        end
    end
    imshow(dif);
    hold on;
    plot(y/n, x/n, 'ro');
    tracks(i, 1) = y/n;
    tracks(i, 2) = x/n;
    M(i) = getframe(gca);
    %input('Hit Return')
end
%myVideo = VideoWriter('track.avi');
%open(myVideo);
%writeVideo(myVideo,M)
%close(myVideo);