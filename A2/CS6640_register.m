function [imr,q,A] = CS6640_register(im,s,cpts)
% CS6640_register - produce registered image and
%     transform
% On input:
%     im (MxN array): input image
%     s (int): transform switch: if 1, then quadratic,
%     else affine
%     cpts (2kx2 array): k corresponding points (evens are one set;
%     odds the other)
% On output:
%     imr (MxN array): registered image
%     q (1x12 vector): quadratic coefficients
%     A (3x3 array): affinetransform
% Call:
%     [imr,q,A] = CS6640_register(im,1);
% Author:
%     Chien-Wei
%     UU
%     Fall 2018
%

[M,N] = size(im);
imr = zeros(M, N);
q = [];
A = [];

if s == 1
    num_cor = size(cpts, 1) / 2;
    b = zeros(2*num_cor,1);
    B = zeros(2*num_cor,12);

    for i = 1:num_cor
        k = i*2-1;
        B(k,:) = [1,cpts(k,1),cpts(k,2),cpts(k,1)*cpts(k,2),...
            cpts(k,1)*cpts(k,1),cpts(k,2)*cpts(k,2),zeros(1,6)];
        B(k+1,:) = [zeros(1,6),1,cpts(k,1),cpts(k,2),...
            cpts(k,1)*cpts(k,2),cpts(k,1)*cpts(k,1),cpts(k,2)*cpts(k,2)];
        l = i*2;
        b(k) = cpts(l,1);
        b(k+1) = cpts(l,2);
    end
    
    q = lsqlin(B,b);
    
    for r = 1:M
        for c = 1:N
            [rp,cp] = CS6640_transform2D(r,c,q);
            rp = uint16(rp);
            cp = uint16(cp);
            if rp >= 1 && cp >= 1 && rp <= M && cp <= N
                imr(rp, cp) = im(r,c);
            end
        end
    end
    imr = mat2gray(imr);
% affine
else
    num_cor = size(cpts, 1) / 2;
    cpts = [cpts ones(num_cor*2,1)];
    b = zeros(2*num_cor,1);
    B = zeros(2*num_cor,6);
    
    for i = 1:num_cor
        k = i*2-1;
        B(k,:) = [cpts(k,1),cpts(k,2),1,zeros(1,3)];
        B(k+1,:) = [zeros(1,3),cpts(k,1),cpts(k,2),1];
        b(k) = cpts(k+1,1);
        b(k+1) = cpts(k+1,2);
    end
    

    q = lsqlin(B,b);
    A = eye(3,3);
    A(1,:) = q(1:3);
    A(2,:) = q(4:6);
    for r = 1:M
        for c = 1:N
            tmp = A * double([r; c; 1]);
            rp = uint16(tmp(1,:));
            cp = uint16(tmp(2,:));
            if rp >= 1 && cp >= 1 && rp <= M && cp <= N
                imr(rp, cp) = im(r,c);
            end
        end
    end
    imr = mat2gray(imr);
    q = [];
end
