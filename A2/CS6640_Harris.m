function H_im = CS6640_Harris(im)
% CS6640_Harris - compute Harris operator at each pixel
% On input:
%     im (MxN array): graylevel image
% On output:
%     H_im (MxN array): Harris value (normalized)
% Call:
%     H = CS6640_Harris(im);
% Author:
%     Chien-Wei
%     UU
%     Fall 2018
%

[M, N] = size(im);
[dx,dy] = gradient(double(im));
[dxx,dxy] = gradient(dx);
[dyx,dyy] = gradient(dy);
H_im = zeros(M, N);

for r = 1:M
    for c = 1:N
        H = [dxx(r,c), dxy(r,c); dyx(r,c), dyy(r,c)];
        H_im(r,c) = uint8(det(H)-0.05*trace(H)*trace(H));
    end
end

H_im = normalize(H_im);
