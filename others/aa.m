y1 = @(x)(1-x)
y2 = @(x)(4-x)/2
y3 = @(x) x-x+2
fplot(y1, 'LineWidth',3)
hold on
fplot(y2, 'LineWidth',3)
fplot(y3, 'LineWidth',3)

axis equal
xlabel('x')
ylabel('y')

y4 = @(x) -x/4 + 2
fplot(y4, 'LineWidth',3)

y5 = @(x) 4.5-x
fplot(y5, 'LineWidth',3)
ylim([-4 6])

txt = '\leftarrow (x, y) = (-2, 3)';
text(-2, 3, txt, 'FontSize',14)