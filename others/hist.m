% matlab histogram
histogram(im)

title('Histogram of the image')
xlabel('Gray level value') 
ylabel('Frequency') 
title('Histogram of a gray level image')
saveas(gcf, 'hist_gray_level.png')

% Negative image
imshow(256.-im)

% Logarithmic
imshow(mat2gray(log(1 + double(im))))

% gammal
imshow(imadjust(im,[],[],2))
% J = imadjust(I,[low_in high_in],[low_out high_out],gamma)

% histogram equalization
histeq(im)

% add noise
imshow(imnoise(im, 'gaussian'))

% Preprocessing Chap3 Gonzalez





