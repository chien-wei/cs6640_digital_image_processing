function [M,tracks] = CS6640_track(video)
% CS6640_track - track motion in a video
% On input:
%     video (video structure): input video
% On output:
%     M (movie structure): movie of detected differences
%     in video
%     tracks (kx2 array): row,col center of mass of largest
%     moving object in sequential video frames
% Call:
%     [M,tr] = CS6640_track(video);
% Author:
%     Chien-Wei Sun
%     UU
%     Fall 2018
%

height = video.Height;
width = video.Width;
frames = zeros(1, height, width);
i = 0;
while hasFrame(video)
    i=i+1;
    vidFrame = readFrame(video);
    frames(i,:,:) = rgb2gray(vidFrame);
end

num_frames = size(frames,1);

% Get backround using mean.
background = zeros(video.Height, video.Width);

for r=1:height
    for c=1:width
        background(r,c) = mean(frames(:, r, c));
    end
end

background = mat2gray(background);

% Get vehicle by subtract the frames to the background.
M(num_frames) = struct('cdata',[],'colormap',[]);
tracks = zeros(num_frames, 2);

for i=1:num_frames
    img = mat2gray(reshape(frames(i, :, :), [height, width]));
    dif = imabsdiff(background, img);
    imshow(dif);
    M(i) = getframe;
    
    % Change to binary image to find center of mass.
    dif = imbinarize(dif,0.25);
    
    x = 0;
    y = 0;
    n = 0;
    for r=1:size(dif,1)
        for c=1:size(dif,2)
            if dif(r,c) == 1
                x = x + r;
                y = y + c;
                n = n + 1;
            end
        end
    end
    
    tracks(i, 1) = x/n;
    tracks(i, 2) = y/n;
end
%movie(M)
%myVideo = VideoWriter('track.avi');
%open(myVideo);
%writeVideo(myVideo,M)
%close(myVideo);