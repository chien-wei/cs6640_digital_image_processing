function T = CS6640_Laws(im)
% CS6640_Laws - compute texture parameters
% On input:
%     im (MxN array): input image
% On output:
%     T (M*Nx10 array): texture parameters
%     each texture parameter is a column vector in T
% Call:
%     T = CS6640_Laws(im);
% Author:
%     Chien-Wei
%     UU
%     Fall 2018
%

[M, N] = size(im);
T = zeros(M*N, 10);

L7 = [1 6 15 20 15 6 1];
E7 = [-1 -4 -5 0 5 4 1];
S7 = [-1 -2 1 4 1 -2 -1];
W7 = [-1 0 3 0 -3 0 1];
R7 = [1 -2 -1 4 -1 -2 1];
O7 = [-1 6 -15 20 -15 6 -1];

L7L7 = L7'*L7;
L7E7 = L7'*E7;
L7S7 = L7'*S7;
L7W7 = L7'*W7;
L7R7 = L7'*R7;
L7O7 = L7'*O7;
E7E7 = E7'*E7;
W7R7 = W7'*R7;
W7O7 = W7'*O7;
MEAN = ones(7,7)./49;

L7L7c = conv2(im, L7L7, 'same');
L7E7c = conv2(im, L7E7, 'same');
L7S7c = conv2(im, L7S7, 'same');
L7W7c = conv2(im, L7W7, 'same');
L7R7c = conv2(im, L7R7, 'same');
L7O7c = conv2(im, L7O7, 'same');
E7E7c = conv2(im, E7E7, 'same');
W7R7c = conv2(im, W7R7, 'same');
W7O7c = conv2(im, W7O7, 'same');
MEANc = conv2(im, MEAN, 'same');


L7L7cS = conv2(abs(L7L7c), MEAN, 'same');
L7E7cS = conv2(abs(L7E7c), MEAN, 'same');
L7S7cS = conv2(abs(L7S7c), MEAN, 'same');
L7W7cS = conv2(abs(L7W7c), MEAN, 'same');
L7R7cS = conv2(abs(L7R7c), MEAN, 'same');
L7O7cS = conv2(abs(L7O7c), MEAN, 'same');
E7E7cS = conv2(abs(E7E7c), MEAN, 'same');
W7R7cS = conv2(abs(W7R7c), MEAN, 'same');
W7O7cS = conv2(abs(W7O7c), MEAN, 'same');
MEANcS = conv2(abs(MEANc), MEAN, 'same');

T(:,1) = L7L7cS(:);
T(:,2) = L7E7cS(:);
T(:,3) = L7S7cS(:);
T(:,4) = L7W7cS(:);
T(:,5) = L7R7cS(:);
T(:,6) = L7O7cS(:);
T(:,7) = E7E7cS(:);
T(:,8) = W7R7cS(:);
T(:,9) = W7O7cS(:);
T(:,10) = MEANcS(:);


