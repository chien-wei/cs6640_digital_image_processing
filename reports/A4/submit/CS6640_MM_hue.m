function M = CS6640_MM_hue(vidObj)
% CS6640_MM_hue - segments moving objects in video
% On input:
%     vidObj (video object obtained by VideoReader): input video
% On output:
%     M (Matlab movie): movie of segmented moving objects
% Call:
%     vidObj = VideoReader('../../../video.avi');
%     M = CS6640_MM(vidObj);
% Author:
%     Kenway Sun
%     Fall 2018
%     UU
%

vidObj = VideoReader('video1.avi');


H = vidObj.Height;
W = vidObj.Width;
background = CS6640_background_hue(vidObj);
vidObj.CurrentTime = 0;

L = vidObj.Duration * vidObj.FrameRate;
frames = zeros(L, H, W);
% Set background as last_fr so it won't look strange.
last_fr = background;
i = 0;
M(L) = struct('cdata',[], 'colormap', []);

B = [0 1 0; 1 1 1; 0 1 0];
F = [0 1 0; 1 0 1; 0 1 0];
D = [1 0 1; 0 0 0; 1 0 1];
R = [1 1 1; 1 1 1; 1 1 1];

while hasFrame(vidObj)
    i=i+1;
    vidFrame = readFrame(vidObj);
    bk_dif = imabsdiff(mat2gray(rgb2gray(vidFrame)), background);
    fr_dif = imabsdiff(mat2gray(rgb2gray(vidFrame)), last_fr);
    %imshow(bk_dif)
    %imshow(fr_dif)
    min_dif = min(bk_dif, fr_dif);
    % need a threshold
    bi = imbinarize(min_dif, 0.15);
    bi = bwareaopen(bi,5);
    
    bi = imdilate(bi, B);
    bi = imdilate(bi, D);
    bi = imdilate(bi, B);
    bi = imdilate(bi, D);
    bi = imdilate(bi, B);
    bi = imdilate(bi, D);
    bi = imdilate(bi, B);
    bi = imdilate(bi, D);
    bi = imdilate(bi, B);

    
    %se = strel('diamond', 5);
    %bi = imclose(bi, se);
    %se = strel('rectangle', [6 6]);
    %bi = imdilate(bi, se);
    %se = strel('rectangle', [6 3]);
    %bi = imdilate(bi, se);
    se = strel('disk', 9);
    %bi = imerode(bi, se);
    bi = imopen(bi, se);
    bi = imclose(bi, se);
    bi = imerode(bi, R);
    bi = imerode(bi, R);
    bi = imerode(bi, R);
    bi = imerode(bi, R);
    
    
    %imshow(bi);
    imshow(mat2gray(rgb2gray(vidFrame)) .* bi);
    M(i) = getframe;
    last_fr = mat2gray(rgb2gray(vidFrame));
    %frames(i,:,:) = rgb2gray(vidFrame);
    %imshow(mat2gray(reshape(frames(i, :, :), [M, N])));
end

