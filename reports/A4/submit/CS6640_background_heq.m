function result = CS6640_background_heq(video)
% CS6640_background_heq - extract background image from video sequence
% histogram equalization version
% On input:
% video (video data structure): cell array of k MxNx3 images
% On output:
% im (MxNx3 array): (uint8)image
% Call:
% im = CS6640_background(v);
% Author:
% Kenway Sun
% UU
% Fall 2018
%

M = video.Height;
N = video.Width;
result = zeros(M, N, 3);
L = video.Duration * video.FrameRate;
% need to translate to int to show the histeq(im) correct.
frames = uint8(zeros(L, M, N, 3));

i = 0;
while hasFrame(video)
    i=i+1;
    frame = readFrame(video);
    frames(i,:,:,:) = reshape(histeq(frame), [1, M, N, 3]);
end

for r=1:M
    for c=1:N
        result(r,c,:) = median(frames(:,r,c,:));
    end
end

result = uint8(result);
